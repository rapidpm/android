package org.rapidpm.android;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import org.rapidpm.android.component.DateTimeView;
import org.rapidpm.android.webservice.AsyncWebServiceTask;
import org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking.*;
import org.rapidpm.webservice.soap.system.security.Benutzer;
import org.rapidpm.webservice.soap.util.RapidWS;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Alexander Vos
 * Date: 04.01.13
 * Time: 15:26
 */
public class IssueDetailFragment extends Fragment {
    private IssueBase mIssue;
    private boolean mNewIssue;

    private List<IssueType> mIssueTypeList;
    private List<IssuePriority> mIssuePriorityList;
    private List<IssueStatus> mIssueStatusList;
    private List<IssueVersion> mIssueVersionList;
    private List<Benutzer> mBenutzerList;

    private ScrollView mScrollView;
    private TextView mTextTextView;
    private EditText mSummaryEditText;
    private Spinner mTypeSpinner;
    private Spinner mPrioritySpinner;
    private Spinner mStatusSpinner;
    private Spinner mVersionSpinner;
    private TextView mReporterTextView;
    private Spinner mAssigneeSpinner;
    private DateTimeView mPlannedDateTimeView;
    private DateTimeView mResolvedDateTimeView;
    private DateTimeView mClosedDateTimeView;
    private EditText mStoryEditText;
    private EditText mRiskEditText;
    private LinearLayout mSubIssuesView;
    private LinearLayout mCommentsView;

    private IssueChangeListener mIssueChangeListener;
    private ActionMode mActionMode;

    public void setIssueChangeListener(final IssueChangeListener issueChangeListener) {
        this.mIssueChangeListener = issueChangeListener;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        mScrollView = (ScrollView) inflater.inflate(R.layout.issue_detail, container, false);

        mTextTextView = (TextView) mScrollView.findViewById(R.id.textText);
        mSummaryEditText = (EditText) mScrollView.findViewById(R.id.summaryEdit);
        mTypeSpinner = (Spinner) mScrollView.findViewById(R.id.typeSpinner);
        mPrioritySpinner = (Spinner) mScrollView.findViewById(R.id.prioritySpinner);
        mStatusSpinner = (Spinner) mScrollView.findViewById(R.id.statusSpinner);
        mVersionSpinner = (Spinner) mScrollView.findViewById(R.id.versionSpinner);
        mReporterTextView = (TextView) mScrollView.findViewById(R.id.reporterText);
        mAssigneeSpinner = (Spinner) mScrollView.findViewById(R.id.assigneeSpinner);
        mPlannedDateTimeView = (DateTimeView) mScrollView.findViewById(R.id.plannedDateView);
        mResolvedDateTimeView = (DateTimeView) mScrollView.findViewById(R.id.resolvedDateView);
        mClosedDateTimeView = (DateTimeView) mScrollView.findViewById(R.id.closedDateView);
        mStoryEditText = (EditText) mScrollView.findViewById(R.id.storyEdit);
        mRiskEditText = (EditText) mScrollView.findViewById(R.id.riskEdit);
        mSubIssuesView = (LinearLayout) mScrollView.findViewById(R.id.subIssuesView);
        mCommentsView = (LinearLayout) mScrollView.findViewById(R.id.commentsView);

        return mScrollView;
    }

    public void updateView() {
        final LayoutInflater inflater = getActivity().getLayoutInflater();

        mIssueTypeList = new ArrayList<IssueType>(IssueActivity.getAllIssueTypes());
        mTypeSpinner.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return mIssueTypeList.size();
            }

            @Override
            public IssueType getItem(final int position) {
                return mIssueTypeList.get(position);
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }

            @Override
            public View getView(final int position, final View convertView, final ViewGroup parent) {
                final View cbView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                final TextView textView = (TextView) cbView.findViewById(android.R.id.text1);
                final IssueType issueType = getItem(position);
                textView.setCompoundDrawablePadding(5);
                textView.setCompoundDrawablesWithIntrinsicBounds(IssueActivity.getIssueTypeIcon(issueType.getId()), 0, 0, 0);
                textView.setText(issueType.getTypeName());
                return textView;
            }
        });

        mIssuePriorityList = new ArrayList<IssuePriority>(IssueActivity.getAllIssuePriorities());
        mPrioritySpinner.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return mIssuePriorityList.size();
            }

            @Override
            public IssuePriority getItem(final int position) {
                return mIssuePriorityList.get(position);
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }

            @Override
            public View getView(final int position, final View convertView, final ViewGroup parent) {
                final View cbView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                final TextView textView = (TextView) cbView.findViewById(android.R.id.text1);
                final IssuePriority issuePriority = getItem(position);
                textView.setCompoundDrawablePadding(5);
                textView.setCompoundDrawablesWithIntrinsicBounds(IssueActivity.getIssuePriorityIcon(issuePriority.getId()), 0, 0, 0);
                textView.setText(issuePriority.getPriorityName());
                return textView;
            }
        });

        mIssueStatusList = new ArrayList<IssueStatus>(IssueActivity.getAllIssueStatuses());
        mStatusSpinner.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return mIssueStatusList.size();
            }

            @Override
            public IssueStatus getItem(final int position) {
                return mIssueStatusList.get(position);
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }

            @Override
            public View getView(final int position, final View convertView, final ViewGroup parent) {
                final View cbView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                final TextView textView = (TextView) cbView.findViewById(android.R.id.text1);
                final IssueStatus issueStatus = getItem(position);
                textView.setCompoundDrawablePadding(5);
                textView.setCompoundDrawablesWithIntrinsicBounds(IssueActivity.getIssueStatusIcon(issueStatus.getId()), 0, 0, 0);
                textView.setText(issueStatus.getStatusName());
                return textView;
            }
        });

        mIssueVersionList = new ArrayList<IssueVersion>(IssueActivity.getAllIssueVersions());
        mVersionSpinner.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return mIssueVersionList.size();
            }

            @Override
            public IssueVersion getItem(final int position) {
                return mIssueVersionList.get(position);
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }

            @Override
            public View getView(final int position, final View convertView, final ViewGroup parent) {
                final View cbView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                final TextView textView = (TextView) cbView.findViewById(android.R.id.text1);
                final IssueVersion issueVersion = getItem(position);
                textView.setText(issueVersion.getVersionName());
                return textView;
            }
        });

        mBenutzerList = new ArrayList<Benutzer>(IssueActivity.getAllBenutzer());
        mAssigneeSpinner.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return mBenutzerList.size();
            }

            @Override
            public Benutzer getItem(final int position) {
                return mBenutzerList.get(position);
            }

            @Override
            public long getItemId(final int position) {
                return position;
            }

            @Override
            public View getView(final int position, final View convertView, final ViewGroup parent) {
                final View cbView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                final TextView textView = (TextView) cbView.findViewById(android.R.id.text1);
                final Benutzer benutzer = getItem(position);
                textView.setText(benutzer.getLogin());
                return textView;
            }
        });
    }

    public IssueBase getIssue() {
        return mIssue;
    }

    public void setIssue(final IssueBase issue, final boolean topLevel) {
        if (mIssue == issue) {
            return;
        }
        if (issue == null) {
            // create new issue
            final IssueBase prevIssue = topLevel ? null : mIssue;
            mIssue = new IssueBase();
            if (prevIssue != null && prevIssue.getId() != null) {
                mIssue.setParentIssue(prevIssue);
            }
            mIssue.setProjectId(IssueActivity.getActiveProject().getId());
            mIssue.setReporterId(MainActivity.getBenutzerId());
            mIssue.setComments(new ArrayList<IssueComment>()); // NPE fix
            mNewIssue = true;
        } else {
            mIssue = issue;
            mNewIssue = false;
        }

        final ActionBar actionBar = getActivity().getActionBar();
        final boolean hasParentIssue = mIssue.hasParentIssue();
        actionBar.setHomeButtonEnabled(hasParentIssue);
        actionBar.setDisplayHomeAsUpEnabled(hasParentIssue);
        if (mNewIssue) {
            actionBar.setTitle(R.string.new_issue);
            actionBar.setSubtitle(mIssue.isTopLevel() ? getString(R.string.top_level_issue) : mIssue.getParentIssue().getText());
        } else {
            actionBar.setTitle(mIssue.getSummary());
            actionBar.setSubtitle(mIssue.getText());
        }

        if (mActionMode != null) {
            mActionMode.finish(); // close the CAB
        }

        mTextTextView.setText(mIssue.getText());
        mSummaryEditText.setText(mIssue.getSummary());

        final Long typeId = mIssue.getTypeId();
        if (typeId != null) {
            final IssueType issueType = IssueActivity.getIssueType(typeId);
            if (issueType != null) {
                mTypeSpinner.setSelection(mIssueTypeList.indexOf(issueType));
            } else {
                Log.w("IssueDetailFragment.setIssue", "issue type not found: " + typeId);
            }
        } else {
            mTypeSpinner.setSelection(0);
        }

        final Long priorityId = mIssue.getPriorityId();
        if (priorityId != null) {
            final IssuePriority issuePriority = IssueActivity.getIssuePriority(priorityId);
            if (issuePriority != null) {
                mPrioritySpinner.setSelection(mIssuePriorityList.indexOf(issuePriority));
            } else {
                Log.w("IssueDetailFragment.setIssue", "issue priority not found: " + priorityId);
            }
        } else {
            mPrioritySpinner.setSelection(0);
        }

        final Long statusId = mIssue.getStatusId();
        if (statusId != null) {
            final IssueStatus issueStatus = IssueActivity.getIssueStatus(statusId);
            if (issueStatus != null) {
                mStatusSpinner.setSelection(mIssueStatusList.indexOf(issueStatus));
            } else {
                Log.w("IssueDetailFragment.setIssue", "issue status not found: " + statusId);
            }
        } else {
            mStatusSpinner.setSelection(0);
        }

        final Long versionId = mIssue.getVersionId();
        if (versionId != null) {
            final IssueVersion issueVersion = IssueActivity.getIssueVersion(versionId);
            if (issueVersion != null) {
                mVersionSpinner.setSelection(mIssueVersionList.indexOf(issueVersion));
            } else {
                Log.w("IssueDetailFragment.setIssue", "issue version not found: " + versionId);
            }
        } else {
            mVersionSpinner.setSelection(0);
        }

        final Long reporterId = mIssue.getReporterId();
        if (reporterId != null) {
            final Benutzer reporter = IssueActivity.getBenutzer(reporterId);
            if (reporter != null) {
                mReporterTextView.setText(reporter.getLogin());
            } else {
                mReporterTextView.setText(R.string.reporter_not_found);
            }
        } else {
            mReporterTextView.setText(R.string.no_reporter);
        }

        final Long assigneeId = mIssue.getAssigneeId();
        if (assigneeId != null) {
            final Benutzer assignee = IssueActivity.getBenutzer(assigneeId);
            if (assignee != null) {
                mAssigneeSpinner.setSelection(mBenutzerList.indexOf(assignee));
            } else {
                Log.w("IssueDetailFragment.setIssue", "Benutzer not found: " + assigneeId);
            }
        } else {
            mAssigneeSpinner.setSelection(0);
        }

        mPlannedDateTimeView.setDateTime(mIssue.getDueDate_planned());
        mResolvedDateTimeView.setDateTime(mIssue.getDueDate_resolved());
        mClosedDateTimeView.setDateTime(mIssue.getDueDate_closed());

        mStoryEditText.setText(mIssue.getStory());

        final Integer risk = mIssue.getRisk();
        if (risk != null) {
            mRiskEditText.setText(risk.toString());
        } else {
            mRiskEditText.getText().clear();
        }

        mSubIssuesView.removeAllViews();
        mCommentsView.removeAllViews();

        if (!mNewIssue) {
            new AsyncWebServiceTask<IssueBase, List<IssueBase>>(getActivity()) {
                @Override
                protected List<IssueBase> callWebService(final IssueBase... params) throws Exception {
                    final IssueBase parentIssue = params[0];
                    final List<IssueBase> subIssues = RapidWS.getIssueTrackingService().getSubIssues(parentIssue.getId());
                    for (final IssueBase subIssue : subIssues) {
                        subIssue.setParentIssue(parentIssue);
                    }
                    return subIssues;
                }

                @Override
                protected void onSuccess(final List<IssueBase> subIssues) {
                    for (final IssueBase subIssue : subIssues) {
                        addSubIssueView(subIssue);
                    }
                }
            }.execute(mIssue);

            for (final IssueComment comment : mIssue.getComments()) {
                addCommentView(comment, false);
            }
        }

        hideKeyboard();
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        });
    }

    private void hideKeyboard() {
        mTextTextView.requestFocus();
        final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mTextTextView.getWindowToken(), 0);
    }

    private void addSubIssueView(final IssueBase subIssue) {
        final View subIssueView = View.inflate(getActivity(), R.layout.subissue, null);
        final TextView nameTextView = (TextView) subIssueView.findViewById(R.id.nameTextView);
        final ImageView priorityImageView = (ImageView) subIssueView.findViewById(R.id.priorityImageView);
        final TextView assigneeTextView = (TextView) subIssueView.findViewById(R.id.assigneeTextView);
        nameTextView.setText(subIssue.getSummary());
        priorityImageView.setImageResource(IssueActivity.getIssuePriorityIcon(subIssue.getPriorityId()));
        final Benutzer assignee = IssueActivity.getBenutzer(subIssue.getAssigneeId());
        assigneeTextView.setText(assignee != null ? assignee.getLogin() : getString(R.string.not_assigned));
        subIssueView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                setIssue(subIssue, false);
            }
        });
        mSubIssuesView.addView(subIssueView);
    }

    private void addCommentView(final IssueComment comment, final boolean newComment) {
        final View commentView = View.inflate(getActivity(), R.layout.issue_comment, null);
        final TextView creatorTextView = (TextView) commentView.findViewById(R.id.creatorTextView);
        final TextView createdTextView = (TextView) commentView.findViewById(R.id.createdTextView);
        final TextView textTextView = (TextView) commentView.findViewById(R.id.textTextView);
        final Benutzer creator = IssueActivity.getBenutzer(comment.getCreatorId());
        final String creatorStr = creator != null ? creator.getLogin() : getString(R.string.anonymous);
        creatorTextView.setText(creatorStr);
        final DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getActivity());
        final String dateStr = dateFormat.format(comment.getCreated());
        createdTextView.setText(dateStr);
        textTextView.setText(comment.getText());
        // can only change or delete own comments for now
        if (comment.getCreatorId().equals(MainActivity.getBenutzerId())) {
            commentView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
                    mActionMode = getActivity().startActionMode(new ActionMode.Callback() {
                        @Override
                        public boolean onCreateActionMode(final ActionMode mode, final Menu menu) {
                            final MenuInflater inflater = mode.getMenuInflater();
                            inflater.inflate(R.menu.comment_menu, menu);
                            return true;
                        }

                        @Override
                        public boolean onPrepareActionMode(final ActionMode mode, final Menu menu) {
                            mode.setTitle(R.string.comment_menu_title);
                            mode.setSubtitle(getString(R.string.comment_menu_subtitle, comment.getId(), creatorStr, dateStr));
                            return true;
                        }

                        @Override
                        public boolean onActionItemClicked(final ActionMode mode, final MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu_change_comment:
                                    showChangeCommentDialog(comment, commentView);
                                    mode.finish();
                                    return true;
                                case R.id.menu_delete_comment:
                                    showDeleteCommentDialog(comment, commentView);
                                    mode.finish();
                                    return true;
                            }
                            return false;
                        }

                        @Override
                        public void onDestroyActionMode(final ActionMode mode) {
                            mActionMode = null;
                        }
                    });
                    return true;
                }
            });
        }
        mCommentsView.addView(commentView);
        if (newComment) {
            // only scroll to the comment if it was recently added
            mScrollView.post(new Runnable() {
                @Override
                public void run() {
                    mScrollView.smoothScrollTo(0, mCommentsView.getTop() + commentView.getBottom());
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.issue_detail_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mIssue != null && mIssue.hasParentIssue()) {
                    setIssue(mIssue.getParentIssue(), false);
                }
                return true;
            case R.id.menu_save_issue:
                saveIssue();
                return true;
            case R.id.menu_add_issue:
                setIssue(null, false);
                return true;
            case R.id.menu_add_comment:
                showAddCommentDialog();
                return true;
            case R.id.menu_refresh:
                ((IssueActivity) getActivity()).reloadProjectData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveIssue() {
        if (mIssue == null) {
            return;
        }

        mIssue.setSummary(mSummaryEditText.getText().toString());
        final IssueType issueType = (IssueType) mTypeSpinner.getSelectedItem();
        if (issueType != null) {
            mIssue.setTypeId(issueType.getId());
        }
        final IssuePriority issuePriority = (IssuePriority) mPrioritySpinner.getSelectedItem();
        if (issuePriority != null) {
            mIssue.setPriorityId(issuePriority.getId());
        }
        final IssueStatus issueStatus = (IssueStatus) mStatusSpinner.getSelectedItem();
        if (issueStatus != null) {
            mIssue.setStatusId(issueStatus.getId());
        }
        final IssueVersion issueVersion = (IssueVersion) mVersionSpinner.getSelectedItem();
        if (issueVersion != null) {
            mIssue.setVersionId(issueVersion.getId());
        }
        final Benutzer assignee = (Benutzer) mAssigneeSpinner.getSelectedItem();
        if (assignee != null) {
            mIssue.setAssigneeId(assignee.getId());
        }
        mIssue.setDueDate_planned(mPlannedDateTimeView.getDateTime());
        mIssue.setDueDate_resolved(mResolvedDateTimeView.getDateTime());
        mIssue.setDueDate_closed(mClosedDateTimeView.getDateTime());
        mIssue.setStory(mStoryEditText.getText().toString());

        try {
            final Integer risk = Integer.valueOf(mRiskEditText.getText().toString());
            mIssue.setRisk(risk);
        } catch (NumberFormatException e) {
            Log.e("IssueDetailFragment.saveIssue", e.getMessage());
        }

        new AsyncWebServiceTask<IssueBase, Long>(getActivity()) {
            @Override
            protected Long callWebService(final IssueBase... params) throws Exception {
                final IssueBase issue = params[0];
                if (mNewIssue && issue.hasParentIssue()) {
                    return RapidWS.getIssueTrackingService().addSubIssue(issue.getParentIssue().getId(), issue);
                } else {
                    return RapidWS.getIssueTrackingService().saveIssue(issue);
                }
            }

            @Override
            protected void onSuccess(final Long id) {
                Toast.makeText(getActivity(), R.string.issue_saved, Toast.LENGTH_SHORT).show();
                if (mNewIssue) {
                    // get issue text (token)
                    new AsyncWebServiceTask<Long, String>(getActivity()) {
                        @Override
                        protected String callWebService(final Long... params) throws Exception {
                            return RapidWS.getIssueTrackingService().getIssueAttribute(params[0], "text", String.class);
                        }

                        @Override
                        protected void onSuccess(final String text) {
                            mIssue.setText(text);
                            mTextTextView.setText(text);
                            if (mIssueChangeListener != null) {
                                mIssueChangeListener.onIssueChanged(mIssue, true);
                            }
                        }
                    }.execute(id);
                    mNewIssue = false;
                } else {
                    if (mIssueChangeListener != null) {
                        mIssueChangeListener.onIssueChanged(mIssue, false);
                    }
                }
            }
        }.execute(mIssue);
    }

    private void showAddCommentDialog() {
        final EditText commentEditText = new EditText(getActivity());
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_input_add)
                .setTitle(R.string.add_comment)
                .setView(commentEditText)
                .setPositiveButton(R.string.add_comment, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        final String commentText = commentEditText.getText().toString().trim();
                        if (!commentText.isEmpty()) {
                            final IssueComment comment = new IssueComment(commentText, MainActivity.getBenutzerId());
                            new AsyncWebServiceTask<IssueComment, Long>(getActivity()) {
                                @Override
                                protected Long callWebService(final IssueComment... params) throws Exception {
                                    return RapidWS.getIssueTrackingService().addOrChangeComment(mIssue.getId(), params[0]);
                                }

                                @Override
                                protected void onSuccess(final Long commentId) {
                                    mIssue.getComments().add(comment);
                                    addCommentView(comment, true);
                                    Toast.makeText(getActivity(), R.string.comment_added, Toast.LENGTH_SHORT).show();
                                }
                            }.execute(comment);
                        }
                    }
                }).setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void showChangeCommentDialog(final IssueComment comment, final View commentView) {
        if (comment == null) {
            return;
        }
        final EditText commentEditText = new EditText(getActivity());
        commentEditText.setText(comment.getText());
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_menu_edit)
                .setTitle(R.string.change_comment)
                .setView(commentEditText)
                .setPositiveButton(R.string.change_comment, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        final String commentText = commentEditText.getText().toString().trim();
                        if (!commentText.isEmpty()) {
                            comment.setText(commentText);
                            new AsyncWebServiceTask<IssueComment, Long>(getActivity()) {
                                @Override
                                protected Long callWebService(final IssueComment... params) throws Exception {
                                    return RapidWS.getIssueTrackingService().addOrChangeComment(mIssue.getId(), params[0]);
                                }

                                @Override
                                protected void onSuccess(final Long commentId) {
                                    final TextView textTextView = (TextView) commentView.findViewById(R.id.textTextView);
                                    textTextView.setText(commentText);
                                    Toast.makeText(getActivity(), R.string.comment_changed, Toast.LENGTH_SHORT).show();
                                }
                            }.execute(comment);
                        }
                    }
                }).setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private void showDeleteCommentDialog(final IssueComment comment, final View commentView) {
        if (comment == null) {
            return;
        }
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_delete)
                .setTitle(R.string.delete_comment)
                .setMessage(R.string.delete_comment_message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        new AsyncWebServiceTask<IssueComment, Boolean>(getActivity()) {
                            @Override
                            protected Boolean callWebService(final IssueComment... params) throws Exception {
                                return RapidWS.getIssueTrackingService().deleteComment(mIssue.getId(), params[0].getId());
                            }

                            @Override
                            protected void onSuccess(final Boolean success) {
                                if (success) {
                                    mIssue.getComments().remove(comment);
                                    mCommentsView.removeView(commentView);
                                    Toast.makeText(getActivity(), R.string.comment_removed, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }.execute(comment);
                    }
                }).setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    public static interface IssueChangeListener {
        void onIssueChanged(IssueBase issue, boolean newIssue);
    }
}
