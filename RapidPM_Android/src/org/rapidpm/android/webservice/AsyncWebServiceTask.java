package org.rapidpm.android.webservice;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import org.rapidpm.android.R;

/**
 * User: Alexander Vos
 * Date: 11.12.12
 * Time: 16:11
 */
public abstract class AsyncWebServiceTask<Params, Result> extends AsyncTask<Params, Void, Result> {
    private ProgressDialog mProgressDialog;
    private Context mContext;
    private CharSequence mTitle;
    private CharSequence mMessage;
    private Exception mError;

    protected AsyncWebServiceTask(final Context context, final CharSequence title, final CharSequence message) {
        this.mContext = context;
        this.mTitle = title;
        this.mMessage = message;
    }

    protected AsyncWebServiceTask(final Context context, final int titleResId, final int messageResId) {
        this.mContext = context;
        this.mTitle = context.getString(titleResId);
        this.mMessage = context.getString(messageResId);
    }

    protected AsyncWebServiceTask(final Context context) {
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        if (mTitle != null) {
            mProgressDialog = ProgressDialog.show(mContext, mTitle, mMessage, true, true,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(final DialogInterface dialog) {
                            AsyncWebServiceTask.this.cancel(true);
                        }
                    });
        }
    }

    protected Result doInBackground(final Params... params) {
        try {
//            if (mTitle != null) {
//                Thread.sleep(2000); // TODO remove debug
//            }
            final Result result = callWebService(params);
            mError = null;
            return result;
        } catch (Exception ex) {
            mError = ex;
            return null;
        }
    }

    protected abstract Result callWebService(final Params... params) throws Exception;

    @Override
    protected void onPostExecute(final Result result) {
        if (mTitle != null) {
            mProgressDialog.dismiss();
        }
        if (mError == null) {
            onSuccess(result);
        } else {
            onError(mError);
        }
    }

    protected abstract void onSuccess(final Result result);

    protected void onError(final Exception error) {
        final String message = error.getMessage();
        if (message != null) {
            Log.e("AsyncWebServiceTask.onError", message);
        }
        if (mTitle != null) {
            final String errorTitle = mContext.getString(R.string.error_title, mTitle);
            new AlertDialog.Builder(mContext)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(errorTitle)
                    .setMessage(message != null ? message : mContext.getString(R.string.unknown_error))
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
        }
    }

    @Override
    protected void onCancelled(final Result result) {
        if (mTitle != null) {
            mProgressDialog.dismiss();
            final String cancelledText = mContext.getString(R.string.cancelled_text, mTitle);
            Toast.makeText(mContext, cancelledText, Toast.LENGTH_SHORT).show();
        }
    }
}
