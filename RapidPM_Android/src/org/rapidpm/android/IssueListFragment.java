package org.rapidpm.android;

import android.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import org.rapidpm.android.webservice.AsyncWebServiceTask;
import org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking.IssueBase;
import org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking.IssueType;
import org.rapidpm.webservice.soap.prj.projectmanagement.planning.PlannedProject;
import org.rapidpm.webservice.soap.util.RapidWS;

import java.util.List;

/**
 * User: Alexander Vos
 * Date: 04.01.13
 * Time: 12:40
 */
public class IssueListFragment extends ListFragment {
    private List<IssueBase> mIssues;
    private IssueChangeListener mIssueChangeListener;

    public void setIssueChangeListener(final IssueChangeListener issueChangeListener) {
        this.mIssueChangeListener = issueChangeListener;
    }

    public void setProject(final PlannedProject project) {
        new AsyncWebServiceTask<PlannedProject, List<IssueBase>>(getActivity(), R.string.loading_top_level_issues, R.string.loading_top_level_issues_message) {
            @Override
            protected List<IssueBase> callWebService(final PlannedProject... params) throws Exception {
                return RapidWS.getIssueTrackingService().getTopLevelIssues(project.getId());
            }

            @Override
            protected void onSuccess(final List<IssueBase> issues) {
                mIssues = issues;
                setListAdapter(new IssueListAdapter());
                if (mIssueChangeListener != null) {
                    mIssueChangeListener.onIssueChanged(mIssues.isEmpty() ? null : mIssues.get(0));
                }
            }
        }.execute(project);
    }

    @Override
    public void onListItemClick(final ListView l, final View v, final int position, final long id) {
        final IssueBase issue = mIssues.get(position);
        if (mIssueChangeListener != null) {
            mIssueChangeListener.onIssueChanged(issue);
        }
    }

    public static interface IssueChangeListener {
        void onIssueChanged(IssueBase issue);
    }

    public class IssueListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return mIssues.size();
        }

        @Override
        public IssueBase getItem(final int position) {
            return mIssues.get(position);
        }

        @Override
        public long getItemId(final int position) {
            return position;
        }

        public void addItem(final IssueBase issue) {
            mIssues.add(issue);
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            final View view;
//            if (convertView == null) {
            final LayoutInflater inflater = getActivity().getLayoutInflater();
            view = inflater.inflate(R.layout.issue_list_row, parent, false);
            final TextView nameTextView = (TextView) view.findViewById(R.id.nameText);
            final TextView subtitleTextView = (TextView) view.findViewById(R.id.subtitleText);

            final IssueBase issue = getItem(position);
            nameTextView.setCompoundDrawablesWithIntrinsicBounds(
                    IssueActivity.getIssueStatusIcon(issue.getStatusId()),
                    0,
                    IssueActivity.getIssuePriorityIcon(issue.getPriorityId()),
                    0);
            nameTextView.setText(issue.getSummary());

            final Long typeId = issue.getTypeId();
            if (typeId != null) {
                final IssueType issueType = IssueActivity.getIssueType(typeId);
                if (issueType != null) {
                    subtitleTextView.setCompoundDrawablesWithIntrinsicBounds(IssueActivity.getIssueTypeIcon(typeId), 0, 0, 0);
                    subtitleTextView.setText(issueType.getTypeName());
                }
            }
//            } else {
//                view = convertView;
//            }
            return view;
        }
    }
}
