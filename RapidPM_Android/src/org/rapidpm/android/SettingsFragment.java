package org.rapidpm.android;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

/**
 * User: Alexander Vos
 * Date: 12.01.13
 * Time: 17:00
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String SERVER_URL_PREF_KEY = "server_url_pref";
    public static final String LOGIN_PREF_KEY = "login_pref";
    public static final String PASSWORD_PREF_KEY = "password_pref";
    public static final String SAVE_LOGIN_PREF_KEY = "save_login_pref";
    public static final String AUTO_LOGIN_PREF_KEY = "auto_login_pref";

    private SharedPreferences mSharedPreferences;

    private EditTextPreference mServerUrlPreference;
    private EditTextPreference mLoginPreference;
    private EditTextPreference mPasswordPreference;
    private SwitchPreference mSaveLoginPreference;
    private SwitchPreference mAutoLoginPreference;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        mSharedPreferences = getPreferenceManager().getSharedPreferences();

        mServerUrlPreference = (EditTextPreference) findPreference(SERVER_URL_PREF_KEY);
        mLoginPreference = (EditTextPreference) findPreference(LOGIN_PREF_KEY);
        mPasswordPreference = (EditTextPreference) findPreference(PASSWORD_PREF_KEY);
        mSaveLoginPreference = (SwitchPreference) findPreference(SAVE_LOGIN_PREF_KEY);
        mAutoLoginPreference = (SwitchPreference) findPreference(AUTO_LOGIN_PREF_KEY);
    }

    @Override
    public void onResume() {
        super.onResume();

        mServerUrlPreference.setSummary(mSharedPreferences.getString(SERVER_URL_PREF_KEY, ""));
        mLoginPreference.setSummary(mSharedPreferences.getString(LOGIN_PREF_KEY, ""));

        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
        if (key.equals(SERVER_URL_PREF_KEY)) {
            mServerUrlPreference.setSummary(mSharedPreferences.getString(SERVER_URL_PREF_KEY, ""));
        } else if (key.equals(LOGIN_PREF_KEY)) {
            mLoginPreference.setSummary(mSharedPreferences.getString(LOGIN_PREF_KEY, ""));
        } else if (key.equals(SAVE_LOGIN_PREF_KEY)) {
            if (!mSaveLoginPreference.isChecked()) {
                mLoginPreference.setText("");
                mPasswordPreference.setText("");
                mAutoLoginPreference.setChecked(false);
            }
        }
    }
}
