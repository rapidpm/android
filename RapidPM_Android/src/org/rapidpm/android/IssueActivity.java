package org.rapidpm.android;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.res.Resources;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;
import org.rapidpm.android.webservice.AsyncWebServiceTask;
import org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking.*;
import org.rapidpm.webservice.soap.prj.projectmanagement.planning.PlannedProject;
import org.rapidpm.webservice.soap.system.security.Benutzer;
import org.rapidpm.webservice.soap.util.RapidWS;

import java.util.*;

/**
 * User: Alexander Vos
 * Date: 04.01.13
 * Time: 12:27
 */
public class IssueActivity extends Activity implements ActionBar.OnNavigationListener {
    private static List<PlannedProject> sProjectList;
    private static PlannedProject sActiveProject;

    private static Map<Long, Benutzer> sBenutzerMap = new HashMap<Long, Benutzer>();

    private static Map<Long, IssuePriority> sIssuePriorityMap = new HashMap<Long, IssuePriority>();
    private static Map<Long, Integer> sIssuePriorityResourceMap = new HashMap<Long, Integer>();
    private static Map<Long, IssueStatus> sIssueStatusMap = new HashMap<Long, IssueStatus>();
    private static Map<Long, Integer> sIssueStatusResourceMap = new HashMap<Long, Integer>();
    private static Map<Long, IssueType> sIssueTypeMap = new HashMap<Long, IssueType>();
    private static Map<Long, Integer> sIssueTypeResourceMap = new HashMap<Long, Integer>();
    private static Map<Long, IssueVersion> sIssueVersionMap = new HashMap<Long, IssueVersion>();

    private IssueListFragment mIssueListFragment;
    private IssueDetailFragment mIssueDetailFragment;

    public static List<PlannedProject> getProjectList() {
        return sProjectList;
    }

    public static PlannedProject getActiveProject() {
        return sActiveProject;
    }

    public void setActiveProject(final PlannedProject project) {
        if (sActiveProject == project) {
            return;
        }
        sActiveProject = project;
        loadProjectData(project);
    }

    public static Collection<Benutzer> getAllBenutzer() {
        return sBenutzerMap.values();
    }

    public static Benutzer getBenutzer(final Long id) {
        // TODO use cache
        return sBenutzerMap.get(id);
    }

    public static Collection<IssuePriority> getAllIssuePriorities() {
        return sIssuePriorityMap.values();
    }

    public static IssuePriority getIssuePriority(final Long id) {
        return sIssuePriorityMap.get(id);
    }

    public static int getIssuePriorityIcon(final Long id) {
        final Integer resId = sIssuePriorityResourceMap.get(id);
        return resId != null ? resId : 0;
    }

    public static Collection<IssueStatus> getAllIssueStatuses() {
        return sIssueStatusMap.values();
    }

    public static IssueStatus getIssueStatus(final Long id) {
        return sIssueStatusMap.get(id);
    }

    public static int getIssueStatusIcon(final Long id) {
        final Integer resId = sIssueStatusResourceMap.get(id);
        return resId != null ? resId : 0;
    }

    public static Collection<IssueType> getAllIssueTypes() {
        return sIssueTypeMap.values();
    }

    public static IssueType getIssueType(final Long id) {
        return sIssueTypeMap.get(id);
    }

    public static int getIssueTypeIcon(final Long id) {
        final Integer resId = sIssueTypeResourceMap.get(id);
        return resId != null ? resId : 0;
    }

    public static Collection<IssueVersion> getAllIssueVersions() {
        return sIssueVersionMap.values();
    }

    public static IssueVersion getIssueVersion(final Long id) {
        // TODO use cache
        return sIssueVersionMap.get(id);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.issue);

        final ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        final FragmentManager fragmentManager = getFragmentManager();
        mIssueListFragment = (IssueListFragment) fragmentManager.findFragmentById(R.id.issue_list_fragment);
        mIssueDetailFragment = (IssueDetailFragment) fragmentManager.findFragmentById(R.id.issue_detail_fragment);

        mIssueListFragment.setIssueChangeListener(new IssueListFragment.IssueChangeListener() {
            @Override
            public void onIssueChanged(final IssueBase issue) {
                mIssueDetailFragment.setIssue(issue, mIssueListFragment.getListAdapter().isEmpty());
            }
        });

        mIssueDetailFragment.setIssueChangeListener(new IssueDetailFragment.IssueChangeListener() {
            @Override
            public void onIssueChanged(final IssueBase issue, final boolean newIssue) {
                actionBar.setTitle(issue.getSummary());
                actionBar.setSubtitle(issue.getText());
                if (issue.isTopLevel()) {
                    final IssueListFragment.IssueListAdapter listAdapter = (IssueListFragment.IssueListAdapter) mIssueListFragment.getListAdapter();
                    if (newIssue) {
                        listAdapter.addItem(issue);
                    } else {
                        listAdapter.notifyDataSetChanged();
                    }
                }
            }
        });

        // load all projects
        new AsyncWebServiceTask<Void, List<PlannedProject>>(this, R.string.loading_projects, R.string.loading_projects_message) {
            @Override
            protected List<PlannedProject> callWebService(final Void... params) throws Exception {
                final List<PlannedProject> projects = RapidWS.getProjectService().getAll();
                if (!projects.isEmpty()) {
                    // TODO extract and cache Benutzer
                    final List<Benutzer> benutzerList = RapidWS.getBenutzerService().getAll();
                    for (final Benutzer b : benutzerList) {
                        sBenutzerMap.put(b.getId(), b);
                    }
                }
                return projects;
            }

            @Override
            protected void onSuccess(final List<PlannedProject> projects) {
                sProjectList = projects;
                if (!projects.isEmpty()) {
                    actionBar.setListNavigationCallbacks(new BaseAdapter() {
                        @Override
                        public int getCount() {
                            return projects.size();
                        }

                        @Override
                        public PlannedProject getItem(final int position) {
                            return projects.get(position);
                        }

                        @Override
                        public long getItemId(final int position) {
                            return position;
                        }

                        @Override
                        public View getView(final int position, final View convertView, final ViewGroup parent) {
                            final View cbView = getLayoutInflater().inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                            final TextView textView = (TextView) cbView.findViewById(android.R.id.text1);
                            final PlannedProject project = getItem(position);
                            textView.setText(project.getProjektName());
                            return textView;
                        }
                    }, IssueActivity.this);
                } else {
                    // TODO no projects found
                }
            }
        }.execute();
    }

    @Override
    public boolean onNavigationItemSelected(final int itemPosition, final long itemId) {
        setActiveProject(sProjectList.get(itemPosition));
        return true;
    }

    private void loadProjectData(final PlannedProject project) {
        if (project == null) {
            return;
        }
        new AsyncWebServiceTask<PlannedProject, Void>(this, R.string.loading_project_data, R.string.loading_project_data_message) {
            @Override
            protected Void callWebService(final PlannedProject... params) throws Exception {
                final IssueTrackingService issueTrackingService = RapidWS.getIssueTrackingService();

                final Long projectId = project.getId();
                final Resources resources = getResources();
                final String drawableDir = "drawable";
                final String packageName = getPackageName();

                final List<IssuePriority> issuePriorities = issueTrackingService.getAllIssuePriorities(projectId);
                sIssuePriorityMap.clear();
                sIssuePriorityResourceMap.clear();
                for (final IssuePriority issuePriority : issuePriorities) {
                    final Long id = issuePriority.getId();
                    sIssuePriorityMap.put(id, issuePriority);
                    final String iconName = removeFileExt(issuePriority.getPriorityFileName());
                    final int iconId = resources.getIdentifier(iconName, drawableDir, packageName);
                    if (iconId != 0) {
                        sIssuePriorityResourceMap.put(id, iconId);
                    } else {
                        Log.w("IssueActivity.setActiveProject", "priority icon resource not found: " + issuePriority.getPriorityFileName());
                    }
                }

                final List<IssueStatus> issueStatuses = issueTrackingService.getAllIssueStatuses(projectId);
                sIssueStatusMap.clear();
                sIssueStatusResourceMap.clear();
                for (final IssueStatus issueStatus : issueStatuses) {
                    final Long id = issueStatus.getId();
                    sIssueStatusMap.put(id, issueStatus);
                    final String iconName = removeFileExt(issueStatus.getStatusFileName());
                    final int iconId = resources.getIdentifier(iconName, drawableDir, packageName);
                    if (iconId != 0) {
                        sIssueStatusResourceMap.put(id, iconId);
                    } else {
                        Log.w("IssueActivity.setActiveProject", "status icon resource not found: " + issueStatus.getStatusFileName());
                    }
                }

                final List<IssueType> issueTypes = issueTrackingService.getAllIssueTypes(projectId);
                sIssueTypeMap.clear();
                sIssueTypeResourceMap.clear();
                for (final IssueType issueType : issueTypes) {
                    final Long id = issueType.getId();
                    sIssueTypeMap.put(id, issueType);
                    final String iconName = removeFileExt(issueType.getTypeFileName());
                    final int iconId = resources.getIdentifier(iconName, drawableDir, packageName);
                    if (iconId != 0) {
                        sIssueTypeResourceMap.put(id, iconId);
                    } else {
                        Log.w("IssueActivity.setActiveProject", "type icon resource not found: " + issueType.getTypeFileName());
                    }
                }

                // TODO cache IssueVersion
                final List<IssueVersion> issueVersions = issueTrackingService.getAllIssueVersions(projectId);
                sIssueVersionMap.clear();
                for (final IssueVersion issueVersion : issueVersions) {
                    sIssueVersionMap.put(issueVersion.getId(), issueVersion);
                }
                return null;
            }

            @Override
            protected void onSuccess(final Void v) {
                mIssueDetailFragment.updateView();
                mIssueListFragment.setProject(project);
            }
        }.execute(project);
    }

    public void reloadProjectData() {
        loadProjectData(sActiveProject);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        super.onCreateOptionsMenu(menu);
        final MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.issue_menu, menu);

        final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint(getString(R.string.search_hint));

        final int maxSearchResults = 10;
        final List<IssueSearchSuggestion> searchSuggestions = new ArrayList<IssueSearchSuggestion>(maxSearchResults);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                new AsyncWebServiceTask<String, List<IssueSearchSuggestion>>(IssueActivity.this) {
                    @Override
                    protected List<IssueSearchSuggestion> callWebService(final String... params) throws Exception {
                        return RapidWS.getIssueTrackingService().search(IssueActivity.getActiveProject().getId(), params[0], maxSearchResults);
                    }

                    @Override
                    protected void onSuccess(final List<IssueSearchSuggestion> issueSearchSuggestions) {
                        searchSuggestions.clear();
                        if (!issueSearchSuggestions.isEmpty()) {
                            searchSuggestions.addAll(issueSearchSuggestions);
                            final MatrixCursor cursor = new MatrixCursor(new String[]{"_id", "token", "summary"}, issueSearchSuggestions.size());
                            for (IssueSearchSuggestion issueSearchSuggestion : issueSearchSuggestions) {
                                cursor.newRow().add(issueSearchSuggestion.getId())
                                        .add(issueSearchSuggestion.getToken())
                                        .add(issueSearchSuggestion.getSummary());
                            }
                            final String[] fromColumnNames = {"summary", "token"};
                            final int[] toResIds = {android.R.id.text1, android.R.id.text2};
                            final CursorAdapter cursorAdapter = new SimpleCursorAdapter(IssueActivity.this, android.R.layout.simple_list_item_2, cursor, fromColumnNames, toResIds, 0);
                            searchView.setSuggestionsAdapter(cursorAdapter);
                        }
                    }
                }.execute(newText);
                return true;
            }
        });
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(final int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(final int position) {
                final IssueSearchSuggestion searchSuggestion = searchSuggestions.get(position);
                new AsyncWebServiceTask<Long, IssueBase>(IssueActivity.this, R.string.loading_issue, R.string.loading_issue_message) {
                    @Override
                    protected IssueBase callWebService(final Long... params) throws Exception {
                        return RapidWS.getIssueTrackingService().findIssueById(params[0]);
                    }

                    @Override
                    protected void onSuccess(final IssueBase issue) {
                        mIssueDetailFragment.setIssue(issue, true);
                    }
                }.execute(searchSuggestion.getId());
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                doLogout();
                return true;
            case R.id.menu_settings:
                MainActivity.showSettings(this);
                return true;
            case R.id.menu_help:
                // TODO
                return true;
            case R.id.menu_about:
                MainActivity.showAbout(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void doLogout() {
        new AsyncWebServiceTask<Void, Void>(this, R.string.logout, R.string.logging_out) {
            @Override
            protected Void callWebService(final Void... params) throws Exception {
                RapidWS.getBenutzerService().logout();
                return null;
            }

            @Override
            protected void onSuccess(final Void aVoid) {
                finish();
            }
        }.execute();
    }

    private static String removeFileExt(final String fileName) {
        if (fileName == null) {
            return null;
        }
        final int pos = fileName.lastIndexOf('.');
        if (pos != -1) {
            return fileName.substring(0, pos);
        } else {
            return fileName;
        }
    }
}
