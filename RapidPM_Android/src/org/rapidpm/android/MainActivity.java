package org.rapidpm.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.rapidpm.android.webservice.AsyncWebServiceTask;
import org.rapidpm.webservice.soap.system.security.Benutzer;
import org.rapidpm.webservice.soap.util.RapidWS;
import org.rapidpm.webservice.soap.util.SoapExecutor;

public class MainActivity extends Activity {
    private static Benutzer sBenutzer;

    public static Benutzer getBenutzer() {
        return sBenutzer;
    }

    public static Long getBenutzerId() {
        return sBenutzer != null ? sBenutzer.getId() : null;
    }

    private EditText mLoginEdit;
    private EditText mPasswordEdit;

    private SharedPreferences mSharedPreferences;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mLoginEdit = (EditText) findViewById(R.id.loginEdit);
        mPasswordEdit = (EditText) findViewById(R.id.passwordEdit);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        final String serverUrl = mSharedPreferences.getString(SettingsFragment.SERVER_URL_PREF_KEY, "");
        SoapExecutor.setServerURL(serverUrl);

        if (mSharedPreferences.getBoolean(SettingsFragment.SAVE_LOGIN_PREF_KEY, false)) {
            final String login = mSharedPreferences.getString(SettingsFragment.LOGIN_PREF_KEY, "");
            final String password = mSharedPreferences.getString(SettingsFragment.PASSWORD_PREF_KEY, "");
            mLoginEdit.setText(login);
            mPasswordEdit.setText(password);
            if (mSharedPreferences.getBoolean(SettingsFragment.AUTO_LOGIN_PREF_KEY, false)) {
                doLogin(login, password);
            }
        }
    }

    public void onLogin(final View view) {
        final String login = mLoginEdit.getText().toString();
        final String password = mPasswordEdit.getText().toString();
        if (mSharedPreferences.getBoolean(SettingsFragment.SAVE_LOGIN_PREF_KEY, false)) {
            final SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(SettingsFragment.LOGIN_PREF_KEY, login);
            editor.putString(SettingsFragment.PASSWORD_PREF_KEY, password);
            editor.commit();
        }
        doLogin(login, password);
    }

    public void doLogin(final String login, final String password) {
        new AsyncWebServiceTask<String, Benutzer>(this, R.string.login, R.string.logging_in) {
            @Override
            protected Benutzer callWebService(final String... params) throws Exception {
                return RapidWS.getBenutzerService().authenticate(params[0], params[1]);
            }

            @Override
            protected void onSuccess(final Benutzer benutzer) {
                MainActivity.sBenutzer = benutzer;

                final String loginStr = getString(R.string.login_successful, benutzer.getLogin());
                Toast.makeText(MainActivity.this, loginStr, Toast.LENGTH_LONG).show();

                final Intent intent = new Intent(MainActivity.this, IssueActivity.class);
                startActivity(intent);
            }
        }.execute(login, password);
    }

    public void onSettings(final View view) {
        showSettings(this);
    }

    public void onAbout(final View view) {
        showAbout(this);
    }

    public static void showSettings(final Context context) {
        final Intent settingsIntent = new Intent(context, SettingsActivity.class);
        context.startActivity(settingsIntent);
    }

    public static void showAbout(final Context context) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(R.string.about)
                .setMessage(context.getString(R.string.copyright_message) + "\n\n"
                        + context.getString(R.string.version) + ' ' + context.getString(R.string.version_string))
                .setPositiveButton(android.R.string.ok, null)
                .setCancelable(true)
                .show();
    }
}
