package org.rapidpm.webservice.soap.util;

import java.util.*;

/**
 * User: Alexander Vos
 * Date: 18.12.12
 * Time: 16:34
 */
public class ClassUtils {
    public static Class<?> boxed2primitive(final Class<?> boxedType) {
        try {
            return (Class<?>) boxedType.getField("TYPE").get(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        System.err.println(boxedType + " is not a boxed type");
        return boxedType;
    }

    private static final Map<Class<?>, Class<?>> primitiveMap = new HashMap<Class<?>, Class<?>>() {{
        put(byte.class, Byte.class);
        put(short.class, Short.class);
        put(int.class, Integer.class);
        put(long.class, Long.class);
        put(float.class, Float.class);
        put(double.class, Double.class);
        put(char.class, Character.class);
        put(boolean.class, Boolean.class);
        put(void.class, Void.class);
    }};

    public static Class<?> primitive2boxed(final Class<?> primitiveType) {
        final Class<?> boxedType = primitiveMap.get(primitiveType);
        if (boxedType != null) {
            return boxedType;
        } else {
            System.err.println(primitiveType + " is not a primitive type");
            return primitiveType;
        }
    }

    public static Collection<Class<?>> getClassHierarchy(Class<?> clazz) {
        final Deque<Class<?>> classHierarchy = new ArrayDeque<Class<?>>();
        while (clazz != null && clazz != Object.class) {
            classHierarchy.addFirst(clazz);
            clazz = clazz.getSuperclass();
        }
        return classHierarchy;
    }

    public static boolean implementsInterface(Class<?> clazz, final Class<?> interfaceClass) {
        if (interfaceClass != null && interfaceClass.isInterface()) {
            if (clazz == interfaceClass) {
                return true;
            }
            while (clazz != null && clazz != Object.class) {
                final Class<?>[] interfaces = clazz.getInterfaces();
                for (final Class<?> anInterface : interfaces) {
                    if (anInterface == interfaceClass) {
                        return true;
                    }
                }
                clazz = clazz.getSuperclass();
            }
        }
        return false;
    }

    private ClassUtils() {
    }
}
