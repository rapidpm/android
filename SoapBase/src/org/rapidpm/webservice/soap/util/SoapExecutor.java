package org.rapidpm.webservice.soap.util;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalDate;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.transport.Transport;
import org.rapidpm.webservice.soap.SoapSerializable;
import org.rapidpm.webservice.soap.converter.SoapConverterRegistry;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Executor zum Absenden eines SOAP-Requests und auswerten der SOAP-Response.
 * <p/>
 * Die SOAP-Response wird durch die Verwendung von SoapSerializable-Objekten automatisch
 * auf die erwartete Klasse gemappt.
 *
 * @see SoapSerializable
 * @see SoapHelper
 */
public class SoapExecutor {

    private static String serverURL = "http://ws.rapidpm.org/";

    public static String getServerURL() {
        return serverURL;
    }

    public static void setServerURL(final String serverURL) {
        SoapExecutor.serverURL = serverURL;
    }

    private final String relativeWebServiceURL;
    private final String operationName;
    private final String namespace;

    /**
     * Konstruktor zum Erstellen eines SoapExecutor mit direkter Bindung an eine SOAP-Operation.
     *
     * @param relativeWebServiceURL Die relative URL zum SOAP-Webservice (Server-URL wird vorangestellt).
     * @param operationName         Der Operationsname der aufzurufenden Remote-Funktion auf dem SOAP-Server.
     * @param namespace             Namespace des SOAP-Webservices.
     * @see #setServerURL(String)
     * @see #execute(Class)
     */
    public SoapExecutor(final String relativeWebServiceURL, final String operationName, final String namespace) {
        this.relativeWebServiceURL = relativeWebServiceURL;
        this.operationName = operationName;
        this.namespace = namespace;
    }

    /**
     * Führt den SOAP-Request aus und mappt die SOAP-Response auf eine Liste der Ergebnisklasse.
     *
     * @param resultClass Ergebnisklasse der SOAP-Response (für automatisches Mapping).
     * @param <R>         Der Typ der Ergebnisklasse der SOAP-Response.
     * @return Voll-instantiierte Liste der Ergebnisobjekte oder eine leere Liste, wenn ein Fehler beim SOAP-Request oder Mapping aufgetreten ist.
     * @throws IOException SoapFault.
     * @see #executeSingle(Class)
     * @see #execute()
     * @see #fillRequest(SoapObject)
     */
    public final <R> List<R> execute(final Class<R> resultClass) throws IOException {
        final List<R> resultList = new ArrayList<R>();

        final long soapCallStartTime = System.nanoTime();
        final SoapObject request = new SoapObject(namespace, operationName);
        fillRequest(request);

        final SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        registerMarshalers(envelope);

        final String webServiceURL = serverURL + relativeWebServiceURL;
        final Transport transport = new HttpTransportSE(webServiceURL);
        transport.debug = true; // TODO Debug-Flag entfernen

        try {
            transport.call(operationName, envelope); // TODO handle SoapFault

            final long soapCallTime = System.nanoTime() - soapCallStartTime;
            final String serviceOperation = relativeWebServiceURL + '#' + operationName;
            System.out.println("soapCallTime(" + serviceOperation + ") " + (soapCallTime / 10e6) + " ms");

            final Object responseObject = envelope.getResponse();
            if (responseObject != null && resultClass != null) {
                final long soapMappingStartTime = System.nanoTime();

                if (responseObject.getClass() == resultClass) {
                    @SuppressWarnings("unchecked")
                    final R result = (R) responseObject;
                    resultList.add(result);
                } else if (responseObject instanceof SoapPrimitive) {
                    final SoapPrimitive soapPrimitive = (SoapPrimitive) responseObject;
                    final R result = SoapConverterRegistry.convert(resultClass, soapPrimitive.toString());
                    resultList.add(result);
                } else {
                    final Vector<SoapObject> responseVector;
                    if (responseObject instanceof SoapObject) {
                        responseVector = new Vector<SoapObject>(1);
                        final SoapObject soapObject = (SoapObject) responseObject;
                        responseVector.add(soapObject);
                    } else { // responseObject instanceof Vector
                        // TODO Vector can also be of type SoapPrimitive?
                        responseVector = (Vector<SoapObject>) responseObject;
                    }
                    for (SoapObject soapObject : responseVector) {
                        if (soapObject != null) {
                            R result = null;
                            try {
                                result = resultClass.newInstance();
                            } catch (InstantiationException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                            if (result != null && result instanceof SoapSerializable) {
                                final SoapSerializable soapSerializable = (SoapSerializable) result;
                                soapSerializable.fromSoap(soapObject);
                                resultList.add(result);
                            }
                        }
                    }
                }
                final long soapMappingTime = System.nanoTime() - soapMappingStartTime;
                System.out.println("soapMappingTime(" + serviceOperation + ") " + (soapMappingTime / 10e6) + " ms");
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    /**
     * Führt den SOAP-Request aus und mappt die SOAP-Response auf die Ergebnisklasse.
     *
     * @param resultClass Ergebnisklasse der SOAP-Response (für automatisches Mapping).
     * @param <R>         Der Typ der Ergebnisklasse der SOAP-Response.
     * @return Voll-instantiiertes Ergebnisobjekt oder <code>null</code>, wenn ein Fehler beim SOAP-Request oder Mapping aufgetreten ist.
     * @throws IOException SoapFault.
     * @see #execute(Class)
     * @see #execute()
     * @see #fillRequest(SoapObject)
     */
    public final <R> R executeSingle(final Class<R> resultClass) throws IOException {
        final List<R> resultList = execute(resultClass);
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList.get(0);
        }
    }

    /**
     * Führt den SOAP-Request aus und igoriert den Rückgabewert.
     *
     * @throws IOException SoapFault.
     * @see #execute(Class)
     * @see #executeSingle(Class)
     */
    public final void execute() throws IOException {
        execute(null);
    }

    /**
     * Wird durch den Executor automatisch aufgerufen, um die SOAP-Request-Parameter setzen zu können.
     *
     * @param request Das SoapObject kann zum Füllen der Request-Parameter verwendet werden.
     */
    protected void fillRequest(SoapObject request) {
        // keine Request-Parameter erforderlich
    }

    protected void registerMarshalers(final SoapSerializationEnvelope envelope) {
        new MarshalDate().register(envelope); // TODO
    }
}
