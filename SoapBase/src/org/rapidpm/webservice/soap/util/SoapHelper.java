package org.rapidpm.webservice.soap.util;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.rapidpm.webservice.soap.SoapSerializable;
import org.rapidpm.webservice.soap.converter.SoapConverter;
import org.rapidpm.webservice.soap.converter.SoapConverterRegistry;

import java.util.*;

/**
 * Hilfsklasse zum Verarbeiten von SOAP-Objekten und deren Eigenschaften.
 */
public class SoapHelper {
    public static <T> T getValue(final SoapObject soapObject, final String name, final Class<T> clazz, final T defaultValue) {
        if (!soapObject.hasProperty(name)) {
            return defaultValue;
        }
        final Object property = soapObject.getProperty(name);
        if (property instanceof SoapPrimitive) {
            final SoapPrimitive soapPrimitive = (SoapPrimitive) property;
            return SoapConverterRegistry.convert(clazz, soapPrimitive.toString());
        } else {
            return defaultValue;
        }
    }

    public static <T> T getValue(final SoapObject soapObject, final String name, final Class<T> clazz) {
        return getValue(soapObject, name, clazz, null);
    }

    public static <T, CT extends Collection<T>> CT getValues(final SoapObject soapObject, final String name, final Class<T> clazz, final CT valueCollection) {
        final SoapConverter<T> converter = SoapConverterRegistry.getConverter(clazz);
        final PropertyInfo propertyInfo = new PropertyInfo();
        for (int i = 0; i < soapObject.getPropertyCount(); i++) {
            soapObject.getPropertyInfo(i, propertyInfo);
            if (name.equals(propertyInfo.getName())) {
                final Object property = propertyInfo.getValue();
                if (property instanceof SoapPrimitive) {
                    final SoapPrimitive soapPrimitive = (SoapPrimitive) property;
                    final T value = converter.convert(soapPrimitive.toString());
                    if (value != null) {
                        valueCollection.add(value);
                    }
                }
            }
        }
        return valueCollection;
    }

    public static <T> List<T> getValuesAsList(final SoapObject soapObject, final String name, final Class<T> clazz) {
        final List<T> valueCollection = new ArrayList<T>();
        return getValues(soapObject, name, clazz, valueCollection);
    }

    public static <T> Set<T> getValuesAsSet(final SoapObject soapObject, final String name, final Class<T> clazz) {
        final Set<T> valueCollection = new HashSet<T>();
        return getValues(soapObject, name, clazz, valueCollection);
    }

    /**
     * Mappt ein SOAP-Objekt auf eine Benutzerdefinierte Klasse.
     *
     * @param soapObject  SOAP-Objekt.
     * @param name        Name der Eigenschaft.
     * @param objectClass Objekt-Klasse der Eigenschaft.
     * @param <T>         Objekt-Typ der Eigenschaft.
     * @return Instanz des Objekts oder <code>null</code>, wenn die Eigenschaft nicht gefunden wurde oder ein Fehler aufgetreten ist.
     */
    public static <T extends SoapSerializable> T getObject(final SoapObject soapObject, final String name, final Class<T> objectClass) {
        if (!soapObject.hasProperty(name)) {
            return null;
        }
        T object = null;
        try {
            object = objectClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if (object != null) {
            final SoapObject property = (SoapObject) soapObject.getProperty(name); // should always be a SoapObject
            object.fromSoap(property);
        }
        return object;
    }

    /**
     * Verarbeitet alle passenden SOAP-Eigenschaften und mappt diese auf eine Collection der angebenen Objekt-Klasse.
     *
     * @param soapObject       SOAP-Objekt.
     * @param name             Name der Eigenschaft(en).
     * @param objectClass      Objekt-Klasse der Eigenschaften.
     * @param objectCollection Collection der Objekt-Klassen.
     * @param <T>              Objekt-Typ der Eigenschaften.
     * @param <CT>             Typ der Object-Collection.
     * @return Collection der gemappten Eigenschaften.
     * @see #getObjectsAsList(SoapObject, String, Class)
     * @see #getObjectsAsSet(SoapObject, String, Class)
     */
    public static <T extends SoapSerializable, CT extends Collection<T>> CT getObjects(final SoapObject soapObject, final String name, final Class<T> objectClass, final CT objectCollection) {
        final PropertyInfo propertyInfo = new PropertyInfo();
        for (int i = 0; i < soapObject.getPropertyCount(); i++) {
            soapObject.getPropertyInfo(i, propertyInfo);
            if (name.equals(propertyInfo.getName())) {
                T object = null;
                try {
                    object = objectClass.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                if (object != null) {
                    final SoapObject property = (SoapObject) propertyInfo.getValue(); // should always be a SoapObject
                    object.fromSoap(property);
                    objectCollection.add(object);
                }
            }
        }
        return objectCollection;
    }

    /**
     * Verarbeitet alle passenden SOAP-Eigenschaften und mappt diese auf eine List der angebenen Objekt-Klasse.
     *
     * @param soapObject  SOAP-Objekt.
     * @param name        Name der Eigenschaft(en).
     * @param objectClass Objekt-Klasse der Eigenschaften.
     * @param <T>         Objekt-Typ der Eigenschaften.
     * @return Liste der gemappten Eigenschaften.
     * @see #getObjects(SoapObject, String, Class, Collection)
     * @see #getObjectsAsSet(SoapObject, String, Class)
     */
    public static <T extends SoapSerializable> List<T> getObjectsAsList(final SoapObject soapObject, final String name, final Class<T> objectClass) {
        final List<T> objectCollection = new ArrayList<T>();
        return getObjects(soapObject, name, objectClass, objectCollection);
    }

    /**
     * Verarbeitet alle passenden SOAP-Eigenschaften und mappt diese auf ein Set der angebenen Objekt-Klasse.
     *
     * @param soapObject  SOAP-Objekt.
     * @param name        Name der Eigenschaft(en).
     * @param objectClass Objekt-Klasse der Eigenschaften.
     * @param <T>         Objekt-Typ der Eigenschaften.
     * @return Set der gemappten Eigenschaften.
     * @see #getObjects(SoapObject, String, Class, Collection)
     * @see #getObjectsAsList(SoapObject, String, Class)
     */
    public static <T extends SoapSerializable> Set<T> getObjectsAsSet(final SoapObject soapObject, final String name, final Class<T> objectClass) {
        final Set<T> objectCollection = new HashSet<T>();
        return getObjects(soapObject, name, objectClass, objectCollection);
    }

    public static void addObject(final SoapObject soapObject, final String name, final SoapSerializable soapSerializable) {
        if (soapSerializable == null) {
            return;
        }
        final SoapObject object = new SoapObject();
        soapSerializable.toSoap(object);
        soapObject.addProperty(name, object);
    }

    public static <T> void addValueCollection(final SoapObject soapObject, final String name, final Collection<T> collection) {
        for (final T value : collection) {
            soapObject.addProperty(name, value);
        }
    }

    public static <T extends SoapSerializable> void addObjectCollection(final SoapObject soapObject, final String name, final Collection<T> collection) {
        for (final T value : collection) {
            addObject(soapObject, name, value);
        }
    }

    private SoapHelper() {
    }
}
