package org.rapidpm.webservice.soap;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;

/**
 * Markiert eine Klasse als (de)serialisierbar von einer SOAP-Webservice-Schnittstelle.
 *
 * @see org.rapidpm.webservice.soap.util.SoapHelper
 * @see org.rapidpm.webservice.soap.util.SoapExecutor
 */
public interface SoapSerializable extends Serializable {

    /**
     * Deserialisiert ein SOAP-Objekt.
     *
     * @param soapObject Das zu deserialisierende SOAP-Objekt.
     */
    void fromSoap(SoapObject soapObject);

    /**
     * Serialisiert ein SOAP-Objekt.
     *
     * @param soapObject Das Zielobjekt der Serialisierung.
     */
    void toSoap(SoapObject soapObject);

}
