package org.rapidpm.webservice.soap.impl;

import org.ksoap2.serialization.SoapObject;
import org.rapidpm.webservice.soap.SoapSerializable;
import org.rapidpm.webservice.soap.converter.SoapConverterRegistry;
import org.rapidpm.webservice.soap.util.ClassUtils;
import org.rapidpm.webservice.soap.util.SoapHelper;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * User: Alexander Vos
 * Date: 09.12.12
 * Time: 10:57
 */
public abstract class ReflectiveSoapSerializable extends AbstractSoapSerializable {

    @Override
    public void fromSoap(final SoapObject soapObject) {
        final Collection<Class<?>> classHierarchy = ClassUtils.getClassHierarchy(getClass());
        for (final Class<?> aClass : classHierarchy) {
            for (final Field field : aClass.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    final Class<?> type = field.getType();
                    final String name = field.getName();
                    Object value;
                    if (ClassUtils.implementsInterface(type, SoapSerializable.class)) {
                        @SuppressWarnings("unchecked")
                        final Class<? extends SoapSerializable> soapSerializableClass = (Class<? extends SoapSerializable>) type;
                        value = SoapHelper.getObject(soapObject, name, soapSerializableClass);
                    } else if (ClassUtils.implementsInterface(type, Collection.class)) {
                        final ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
                        final Class<?> genericType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                        if (ClassUtils.implementsInterface(genericType, SoapSerializable.class)) {
                            @SuppressWarnings("unchecked")
                            final Class<? extends SoapSerializable> soapSerializableClass = (Class<? extends SoapSerializable>) genericType;
                            if (ClassUtils.implementsInterface(type, List.class)) {
                                value = SoapHelper.getObjectsAsList(soapObject, name, soapSerializableClass);
                            } else if (ClassUtils.implementsInterface(type, Set.class)) {
                                value = SoapHelper.getObjectsAsSet(soapObject, name, soapSerializableClass);
                            } else {
                                System.err.println("Unknown collection type: " + type);
                                continue;
                            }
                        } else { // should be a primitive type
                            if (ClassUtils.implementsInterface(type, List.class)) {
                                value = SoapHelper.getValuesAsList(soapObject, name, genericType);
                            } else if (ClassUtils.implementsInterface(type, Set.class)) {
                                value = SoapHelper.getValuesAsSet(soapObject, name, genericType);
                            } else {
                                System.err.println("Unknown collection type: " + type);
                                continue;
                            }
                        }
                    } else { // should be a primitive type
                        value = SoapHelper.getValue(soapObject, name, type);
                        if (value == null && field.isAnnotationPresent(DefaultValue.class)) {
                            String defaultValueStr = field.getAnnotation(DefaultValue.class).value();
                            value = SoapConverterRegistry.convert(type, defaultValueStr);
                        }
                    }
                    field.setAccessible(true);
                    try {
                        field.set(this, value);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void toSoap(final SoapObject soapObject) {
        final Collection<Class<?>> classHierarchy = ClassUtils.getClassHierarchy(getClass());
        for (final Class<?> aClass : classHierarchy) {
            for (final Field field : aClass.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    final String name = field.getName();
                    field.setAccessible(true);
                    final Object value;
                    try {
                        value = field.get(this);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        continue;
                    }
                    if (value == null) {
                        //continue;
                    } else if (value instanceof SoapSerializable) {
                        final SoapSerializable soapSerializable = (SoapSerializable) value;
                        SoapHelper.addObject(soapObject, name, soapSerializable);
                    } else if (value instanceof Collection<?>) {
                        final Collection<?> collection = (Collection<?>) value;
                        for (final Object obj : collection) {
                            if (obj == null) {
                                //continue;
                            } else if (obj instanceof SoapSerializable) {
                                final SoapSerializable soapSerializable = (SoapSerializable) obj;
                                SoapHelper.addObject(soapObject, name, soapSerializable);
                            } else { // should be a primitive type
                                soapObject.addProperty(name, obj);
                            }
                        }
                    } else { // should be a primitive type
                        soapObject.addProperty(name, value);
                    }
                }
            }
        }
    }
}
