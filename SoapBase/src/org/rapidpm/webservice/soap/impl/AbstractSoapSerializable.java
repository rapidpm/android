package org.rapidpm.webservice.soap.impl;

import org.ksoap2.serialization.SoapObject;
import org.rapidpm.webservice.soap.SoapSerializable;
import org.rapidpm.webservice.soap.util.SoapHelper;

/**
 * User: Alexander Vos
 * Date: 09.12.12
 * Time: 10:57
 */
public abstract class AbstractSoapSerializable implements SoapSerializable {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public void fromSoap(final SoapObject soapObject) {
        id = SoapHelper.getValue(soapObject, "id", Long.class);
    }

    @Override
    public void toSoap(final SoapObject soapObject) {
        soapObject.addPropertyIfValue("id", id);
    }
}
