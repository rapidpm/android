package org.rapidpm.webservice.soap.converter;

/**
 * User: Alexander Vos
 * Date: 18.12.12
 * Time: 16:19
 */
public interface SoapConverter<T> {
    T convert(String value);
    String toString(T value);
}
