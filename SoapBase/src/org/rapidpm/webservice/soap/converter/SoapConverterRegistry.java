package org.rapidpm.webservice.soap.converter;

import org.rapidpm.webservice.soap.converter.impl.*;
import org.rapidpm.webservice.soap.util.ClassUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Alexander Vos
 * Date: 18.12.12
 * Time: 16:20
 */
public class SoapConverterRegistry {
    private static final Map<Class<?>, SoapConverter<?>> converterMap = new HashMap<Class<?>, SoapConverter<?>>();

    public static <T> void registerConverter(final Class<T> clazz, final SoapConverter<T> converter) {
        converterMap.put(clazz, converter);
    }

    public static void unregisterConverter(final Class<?> clazz) {
        converterMap.remove(clazz);
    }

    public static boolean hasConverter(Class<?> clazz) {
        if (clazz.isPrimitive()) {
            clazz = ClassUtils.primitive2boxed(clazz);
        }
        return converterMap.containsKey(clazz);
    }

    @SuppressWarnings("unchecked")
    public static <T> SoapConverter<T> getConverter(Class<T> clazz) {
        if (clazz.isPrimitive()) {
            clazz = (Class<T>) ClassUtils.primitive2boxed(clazz);
        }
        return (SoapConverter<T>) converterMap.get(clazz);
    }

    public static <T> T convert(final Class<T> clazz, final String value) {
        final SoapConverter<T> converter = getConverter(clazz);
        if (converter != null) {
            return converter.convert(value);
        } else {
            System.err.println("No SOAP converter found for type " + clazz);
            return null;
        }
    }

    public static <T> String toString(final Class<T> clazz, final T value) {
        final SoapConverter<T> converter = getConverter(clazz);
        if (converter != null) {
            return converter.toString(value);
        } else {
            System.err.println("No SOAP converter found for type " + clazz);
            return null;
        }
    }

    public static <T> String toString(final T value) {
        @SuppressWarnings("unchecked")
        final Class<T> clazz = (Class<T>) value.getClass();
        return toString(clazz, value);
    }

    private SoapConverterRegistry() {
    }

    static {
        registerConverter(String.class, new StringSoapConverter());
        registerConverter(Long.class, new LongSoapConverter());
        registerConverter(Integer.class, new IntegerSoapConverter());
        registerConverter(Boolean.class, new BooleanSoapConverter());
        registerConverter(Date.class, new DateSoapConverter());
        // TODO
    }
}
