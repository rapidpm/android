package org.rapidpm.webservice.soap.converter.impl;

import org.rapidpm.webservice.soap.converter.SoapConverter;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 13:25
 */
public abstract class AbstractSoapConverter<T> implements SoapConverter<T> {
    @Override
    public String toString(final T value) {
        if (value != null) {
            return value.toString();
        } else {
            return null;
        }
    }
}
