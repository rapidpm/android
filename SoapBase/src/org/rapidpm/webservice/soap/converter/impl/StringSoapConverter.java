package org.rapidpm.webservice.soap.converter.impl;

import org.rapidpm.webservice.soap.converter.SoapConverter;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 12:13
 */
public class StringSoapConverter implements SoapConverter<String> {
    @Override
    public String convert(final String value) {
        return value;
    }

    @Override
    public String toString(final String value) {
        return value;
    }
}
