package org.rapidpm.webservice.soap.converter.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 12:14
 */
public class DateSoapConverter extends AbstractSoapConverter<Date> {
    public static final SimpleDateFormat SOAP_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Override
    public Date convert(final String value) {
        try {
            return SOAP_DATE_FORMAT.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString(final Date value) {
        return SOAP_DATE_FORMAT.format(value);
    }
}
