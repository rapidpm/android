package org.rapidpm.webservice.soap.converter.impl;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 12:14
 */
public class BooleanSoapConverter extends AbstractSoapConverter<Boolean> {
    @Override
    public Boolean convert(final String value) {
        if (value != null && !value.isEmpty()) {
            return Boolean.valueOf(value);
        } else {
            return null;
        }
    }
}
