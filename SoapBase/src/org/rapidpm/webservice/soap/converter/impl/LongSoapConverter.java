package org.rapidpm.webservice.soap.converter.impl;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 12:14
 */
public class LongSoapConverter extends AbstractSoapConverter<Long> {
    @Override
    public Long convert(final String value) {
        if (value != null && !value.isEmpty()) {
            return Long.valueOf(value);
        } else {
            return null;
        }
    }
}
