package org.rapidpm.webservice.soap.converter.impl;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 12:14
 */
public class IntegerSoapConverter extends AbstractSoapConverter<Integer> {
    @Override
    public Integer convert(final String value) {
        if (value != null && !value.isEmpty()) {
            return Integer.valueOf(value);
        } else {
            return null;
        }
    }
}
