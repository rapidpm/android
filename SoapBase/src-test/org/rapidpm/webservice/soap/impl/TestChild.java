package org.rapidpm.webservice.soap.impl;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 12:50
 */
public class TestChild extends ReflectiveSoapSerializable {
    @DefaultValue("123")
    private long primitiveLong;
    @DefaultValue("321")
    private int primitiveInt;

    public long getPrimitiveLong() {
        return primitiveLong;
    }

    public void setPrimitiveLong(final long primitiveLong) {
        this.primitiveLong = primitiveLong;
    }

    public int getPrimitiveInt() {
        return primitiveInt;
    }

    public void setPrimitiveInt(final int primitiveInt) {
        this.primitiveInt = primitiveInt;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final TestChild testChild = (TestChild) o;

        if (id != null ? !id.equals(testChild.id) : testChild.id != null) return false;
        if (primitiveInt != testChild.primitiveInt) return false;
        if (primitiveLong != testChild.primitiveLong) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TestChild");
        sb.append("{id=").append(id);
        sb.append(", primitiveLong=").append(primitiveLong);
        sb.append(", primitiveInt=").append(primitiveInt);
        sb.append('}');
        return sb.toString();
    }
}
