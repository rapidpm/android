package org.rapidpm.webservice.soap.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 12:50
 */
public class TestParent extends ReflectiveSoapSerializable {
    @DefaultValue("hello")
    private String string;
    @DefaultValue("777")
    private Long aLong;
    @DefaultValue("-43")
    private Integer anInt;
    private Date date;
    private Set<Long> longSet;
    private List<String> stringList;
    private List<TestChild> childList;
    private Set<TestChild> childSet;

    public String getString() {
        return string;
    }

    public void setString(final String string) {
        this.string = string;
    }

    public Long getaLong() {
        return aLong;
    }

    public void setaLong(final Long aLong) {
        this.aLong = aLong;
    }

    public Integer getAnInt() {
        return anInt;
    }

    public void setAnInt(final Integer anInt) {
        this.anInt = anInt;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public Set<Long> getLongSet() {
        return longSet;
    }

    public void setLongSet(final Set<Long> longSet) {
        this.longSet = longSet;
    }

    public List<String> getStringList() {
        return stringList;
    }

    public void setStringList(final List<String> stringList) {
        this.stringList = stringList;
    }

    public List<TestChild> getChildList() {
        return childList;
    }

    public void setChildList(final List<TestChild> childList) {
        this.childList = childList;
    }

    public Set<TestChild> getChildSet() {
        return childSet;
    }

    public void setChildSet(final Set<TestChild> childSet) {
        this.childSet = childSet;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final TestParent that = (TestParent) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (aLong != null ? !aLong.equals(that.aLong) : that.aLong != null) return false;
        if (anInt != null ? !anInt.equals(that.anInt) : that.anInt != null) return false;
        if (childList != null ? !childList.equals(that.childList) : that.childList != null) return false;
        if (childSet != null ? !childSet.equals(that.childSet) : that.childSet != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (longSet != null ? !longSet.equals(that.longSet) : that.longSet != null) return false;
        if (string != null ? !string.equals(that.string) : that.string != null) return false;
        if (stringList != null ? !stringList.equals(that.stringList) : that.stringList != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TestParent");
        sb.append("{id=").append(id);
        sb.append(", string='").append(string).append('\'');
        sb.append(", aLong=").append(aLong);
        sb.append(", anInt=").append(anInt);
        sb.append(", date=").append(date);
        sb.append(", longSet=").append(longSet);
        sb.append(", stringList=").append(stringList);
        sb.append(", childList=").append(childList);
        sb.append(", childSet=").append(childSet);
        sb.append('}');
        return sb.toString();
    }
}
