package org.rapidpm.webservice.soap.impl;

import org.junit.Test;
import org.ksoap2.serialization.SoapObject;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * User: Alexander Vos
 * Date: 19.12.12
 * Time: 00:27
 */
public class ReflectiveSoapSerializableTest {

    private static TestParent createTestObject() {
        final TestParent parent = new TestParent();
        parent.setId(42L);
        parent.setString("foo bar");
        parent.setaLong(null); // use default value
        parent.setAnInt(null); // use default value
        parent.setDate(new Date());
        parent.setLongSet(new HashSet<Long>(Arrays.asList(1L, 2L, 4L, 8L)));
        parent.setStringList(Arrays.asList("foo", "bar", "baz"));
        final TestChild child1 = new TestChild();
        child1.setId(1L);
        child1.setPrimitiveLong(2L);
        child1.setPrimitiveInt(3);
        final TestChild child2 = new TestChild();
        child2.setId(2L);
        child2.setPrimitiveLong(20L);
        child2.setPrimitiveInt(30);
        final Collection<TestChild> children = Arrays.asList(child1, child2);
        parent.setChildList(new ArrayList<TestChild>(children));
        parent.setChildSet(new HashSet<TestChild>(children));
        return parent;
    }

    @Test
    public void testFromSoap() throws Exception {
        final TestParent testObject = createTestObject();
        final SoapObject soapObject = new SoapObject();
        testObject.toSoap(soapObject);

        final TestParent testParent = new TestParent();
        testParent.fromSoap(soapObject);

        assertEquals(testObject, testParent);
    }

    @Test
    public void testToSoap() throws Exception {
        final TestParent testObject = createTestObject();
        final SoapObject soapObject = new SoapObject();
        testObject.toSoap(soapObject);

        System.out.println(soapObject);
    }
}
