package org.rapidpm.webservice.soap.util;

import org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking.IssueTrackingService;
import org.rapidpm.webservice.soap.prj.projectmanagement.planning.PlannedProjectService;
import org.rapidpm.webservice.soap.system.security.BenutzerService;

/**
 * User: Alexander Vos
 * Date: 23.01.13
 * Time: 14:44
 */
public class RapidWS {
    private static BenutzerService benutzerService;
    private static PlannedProjectService projectService;
    private static IssueTrackingService issueTrackingService;

    public static BenutzerService getBenutzerService() {
        if (benutzerService == null) {
            benutzerService = new BenutzerService();
        }
        return benutzerService;
    }

    public static PlannedProjectService getProjectService() {
        if (projectService == null) {
            projectService = new PlannedProjectService();
        }
        return projectService;
    }

    public static IssueTrackingService getIssueTrackingService() {
        if (issueTrackingService == null) {
            issueTrackingService = new IssueTrackingService();
        }
        return issueTrackingService;
    }

    private RapidWS() {
    }
}
