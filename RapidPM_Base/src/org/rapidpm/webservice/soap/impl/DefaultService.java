package org.rapidpm.webservice.soap.impl;

import org.ksoap2.serialization.SoapObject;
import org.rapidpm.webservice.soap.BaseService;
import org.rapidpm.webservice.soap.util.SoapExecutor;
import org.rapidpm.webservice.soap.util.SoapHelper;

import java.io.IOException;
import java.util.List;

/**
 * User: Alexander Vos
 * Date: 09.12.12
 * Time: 10:56
 */
public abstract class DefaultService<T extends AbstractSoapSerializable> extends BaseService<T> {

    public DefaultService(final String relativeWebServiceURL, final String namespace, final Class<T> objectClass) {
        super(relativeWebServiceURL, namespace, objectClass);
    }

    public T findById(final Long id) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "findById", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("id", id);
            }
        }.executeSingle(objectClass);
    }

    public List<T> findByIdList(final List<Long> idList) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "findByIdList", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                SoapHelper.addValueCollection(request, "idList", idList);
            }
        }.execute(objectClass);
    }

    public List<T> getAll() throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getAll", namespace)
                .execute(objectClass);
    }

    public Long save(final T obj) throws IOException {
        final Long id = new SoapExecutor(relativeWebServiceURL, "save", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                SoapHelper.addObject(request, "entity", obj);
            }
        }.executeSingle(Long.class);
        obj.setId(id);
        return id;
    }

    public boolean delete(final Long id) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "delete", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("id", id);
            }
        }.executeSingle(Boolean.class);
    }
}
