package org.rapidpm.webservice.soap.system.security.berechtigungen;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

public class Berechtigung extends ReflectiveSoapSerializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Berechtigung");
        sb.append("{id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
