package org.rapidpm.webservice.soap.system.security;

import org.ksoap2.serialization.SoapObject;
import org.rapidpm.webservice.soap.impl.DefaultService;
import org.rapidpm.webservice.soap.util.SoapExecutor;

import java.io.IOException;

/**
 * User: Alexander Vos
 * Date: 09.12.12
 * Time: 11:24
 */
public class BenutzerService extends DefaultService<Benutzer> {
    public BenutzerService() {
        super("BenutzerWS", "http://security.system.persistence.webservice.rapidpm.org/", Benutzer.class);
    }

    public Benutzer authenticate(final String login, final String passwd) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "authenticate", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("login", login);
                request.addProperty("passwd", passwd);
            }
        }.executeSingle(Benutzer.class);
    }

    public void logout() throws IOException {
        new SoapExecutor(relativeWebServiceURL, "logout", namespace)
                .execute();
    }
}
