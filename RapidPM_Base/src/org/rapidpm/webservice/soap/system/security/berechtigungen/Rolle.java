package org.rapidpm.webservice.soap.system.security.berechtigungen;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

import java.util.Set;

public class Rolle extends ReflectiveSoapSerializable {
    private String name;
    private Set<Berechtigung> berechtigungen;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Set<Berechtigung> getBerechtigungen() {
        return berechtigungen;
    }

    public void setBerechtigungen(final Set<Berechtigung> berechtigungen) {
        this.berechtigungen = berechtigungen;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Rolle");
        sb.append("{id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", berechtigungen=").append(berechtigungen);
        sb.append('}');
        return sb.toString();
    }
}
