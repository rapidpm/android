package org.rapidpm.webservice.soap.system.security;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;
import org.rapidpm.webservice.soap.system.security.berechtigungen.Rolle;

import java.util.Set;

/**
 * User: Alexander Vos
 * Date: 08.12.12
 * Time: 14:12
 */
public class Benutzer extends ReflectiveSoapSerializable {
    private String login;
    private String email;
    private Set<Rolle> rollen;

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public Set<Rolle> getRollen() {
        return rollen;
    }

    public void setRollen(final Set<Rolle> rollen) {
        this.rollen = rollen;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Benutzer");
        sb.append("{id=").append(id);
        sb.append(", login='").append(login).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", rollen=").append(rollen);
        sb.append('}');
        return sb.toString();
    }
}
