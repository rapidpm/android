package org.rapidpm.webservice.soap;

import org.rapidpm.webservice.soap.impl.AbstractSoapSerializable;

/**
 * User: Alexander Vos
 * Date: 09.12.12
 * Time: 10:56
 */
public abstract class BaseService<T extends AbstractSoapSerializable> {

    protected final String relativeWebServiceURL;
    protected final String namespace;
    protected final Class<T> objectClass;

    public BaseService(final String relativeWebServiceURL, final String namespace, final Class<T> objectClass) {
        this.relativeWebServiceURL = relativeWebServiceURL;
        this.namespace = namespace;
        this.objectClass = objectClass;
    }
}
