package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

/**
 * User: Alexander Vos
 * Date: 18.12.12
 * Time: 11:11
 */
public class IssueType extends ReflectiveSoapSerializable {
    private Long projectId;
    private String typeName;
    private String typeFileName;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(final Long projectId) {
        this.projectId = projectId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(final String typeName) {
        this.typeName = typeName;
    }

    public String getTypeFileName() {
        return typeFileName;
    }

    public void setTypeFileName(final String typeFileName) {
        this.typeFileName = typeFileName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssueType");
        sb.append("{id=").append(id);
        sb.append(", projectId=").append(projectId);
        sb.append(", typeName='").append(typeName).append('\'');
        sb.append(", typeFileName='").append(typeFileName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
