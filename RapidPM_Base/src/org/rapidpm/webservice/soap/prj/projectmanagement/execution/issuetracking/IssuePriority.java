package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

/**
 * User: Alexander Vos
 * Date: 18.12.12
 * Time: 11:10
 */
public class IssuePriority extends ReflectiveSoapSerializable {
    private Long projectId;
    private Integer prio;
    private String priorityName;
    private String priorityFileName;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(final Long projectId) {
        this.projectId = projectId;
    }

    public Integer getPrio() {
        return prio;
    }

    public void setPrio(final Integer prio) {
        this.prio = prio;
    }

    public String getPriorityName() {
        return priorityName;
    }

    public void setPriorityName(final String priorityName) {
        this.priorityName = priorityName;
    }

    public String getPriorityFileName() {
        return priorityFileName;
    }

    public void setPriorityFileName(final String priorityFileName) {
        this.priorityFileName = priorityFileName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssuePriority");
        sb.append("{id=").append(id);
        sb.append(", projectId=").append(projectId);
        sb.append(", prio=").append(prio);
        sb.append(", priorityName='").append(priorityName).append('\'');
        sb.append(", priorityFileName='").append(priorityFileName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
