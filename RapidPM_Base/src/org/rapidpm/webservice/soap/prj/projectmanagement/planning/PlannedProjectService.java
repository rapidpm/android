package org.rapidpm.webservice.soap.prj.projectmanagement.planning;

import org.rapidpm.webservice.soap.impl.DefaultService;
import org.rapidpm.webservice.soap.util.SoapExecutor;

import java.io.IOException;

/**
 * User: Alexander Vos
 * Date: 13.12.12
 * Time: 15:27
 */
public class PlannedProjectService extends DefaultService<PlannedProject> {
    public PlannedProjectService() {
        super("PlannedProjectWS", "http://planning.projectmanagement.prj.persistence.webservice.rapidpm.org/", PlannedProject.class);
    }

    public PlannedProject getFirstProject() throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getFirstProject", namespace)
                .executeSingle(PlannedProject.class);
    }
}
