package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

/**
 * User: Alexander Vos
 * Date: 22.01.13
 * Time: 12:48
 */
public class IssueSearchSuggestion extends ReflectiveSoapSerializable {
    private String token;
    private String summary;

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(final String summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssueSearchSuggestion");
        sb.append("{id=").append(id);
        sb.append(", token='").append(token).append('\'');
        sb.append(", summary='").append(summary).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
