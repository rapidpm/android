package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

import java.util.Date;
import java.util.List;

/**
 * User: Alexander Vos
 * Date: 13.12.12
 * Time: 15:29
 */
public class IssueBase extends ReflectiveSoapSerializable {
    private String text;
    private Long projectId;
    private String summary;
    private String story;
    private Long priorityId;
    private Long statusId;
    private Long typeId;
    private Long reporterId;
    private Long assigneeId;
    private Long versionId;
    private Date dueDate_planned;
    private Date dueDate_resolved;
    private Date dueDate_closed;
    private IssueTimeUnit timeUnitEstimated;
    private List<IssueTimeUnit> timeUnitsUsed;
    private List<IssueComment> comments;
    private Integer risk;

    private IssueBase parentIssue;

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(final Long projectId) {
        this.projectId = projectId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(final String summary) {
        this.summary = summary;
    }

    public String getStory() {
        return story;
    }

    public void setStory(final String story) {
        this.story = story;
    }

    public Long getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(final Long priorityId) {
        this.priorityId = priorityId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(final Long statusId) {
        this.statusId = statusId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(final Long typeId) {
        this.typeId = typeId;
    }

    public Long getReporterId() {
        return reporterId;
    }

    public void setReporterId(final Long reporterId) {
        this.reporterId = reporterId;
    }

    public Long getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(final Long assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(final Long versionId) {
        this.versionId = versionId;
    }

    public Date getDueDate_planned() {
        return dueDate_planned;
    }

    public void setDueDate_planned(final Date dueDate_planned) {
        this.dueDate_planned = dueDate_planned;
    }

    public Date getDueDate_resolved() {
        return dueDate_resolved;
    }

    public void setDueDate_resolved(final Date dueDate_resolved) {
        this.dueDate_resolved = dueDate_resolved;
    }

    public Date getDueDate_closed() {
        return dueDate_closed;
    }

    public void setDueDate_closed(final Date dueDate_closed) {
        this.dueDate_closed = dueDate_closed;
    }

    public IssueTimeUnit getTimeUnitEstimated() {
        return timeUnitEstimated;
    }

    public void setTimeUnitEstimated(final IssueTimeUnit timeUnitEstimated) {
        this.timeUnitEstimated = timeUnitEstimated;
    }

    public List<IssueTimeUnit> getTimeUnitsUsed() {
        return timeUnitsUsed;
    }

    public void setTimeUnitsUsed(final List<IssueTimeUnit> timeUnitsUsed) {
        this.timeUnitsUsed = timeUnitsUsed;
    }

    public List<IssueComment> getComments() {
        return comments;
    }

    public void setComments(final List<IssueComment> comments) {
        this.comments = comments;
    }

    public Integer getRisk() {
        return risk;
    }

    public void setRisk(final Integer risk) {
        this.risk = risk;
    }

    public IssueBase getParentIssue() {
        return parentIssue;
    }

    public void setParentIssue(final IssueBase parentIssue) {
        this.parentIssue = parentIssue;
    }

    public boolean hasParentIssue() {
        return parentIssue != null;
    }

    public boolean isTopLevel() {
        return parentIssue == null;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssueBase");
        sb.append("{id=").append(id);
        sb.append(", text='").append(text).append('\'');
        sb.append(", projectId=").append(projectId);
        sb.append(", summary='").append(summary).append('\'');
        sb.append(", story='").append(story).append('\'');
        sb.append(", priorityId=").append(priorityId);
        sb.append(", statusId=").append(statusId);
        sb.append(", typeId=").append(typeId);
        sb.append(", reporterId=").append(reporterId);
        sb.append(", assigneeId=").append(assigneeId);
        sb.append(", versionId=").append(versionId);
        sb.append(", dueDate_planned=").append(dueDate_planned);
        sb.append(", dueDate_resolved=").append(dueDate_resolved);
        sb.append(", dueDate_closed=").append(dueDate_closed);
        sb.append(", timeUnitEstimated=").append(timeUnitEstimated);
        sb.append(", timeUnitsUsed=").append(timeUnitsUsed);
        sb.append(", comments=").append(comments);
        sb.append(", risk=").append(risk);
        sb.append('}');
        return sb.toString();
    }
}
