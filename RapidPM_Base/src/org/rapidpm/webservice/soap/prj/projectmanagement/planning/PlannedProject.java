package org.rapidpm.webservice.soap.prj.projectmanagement.planning;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

/**
 * User: Alexander Vos
 * Date: 13.12.12
 * Time: 15:28
 */
public class PlannedProject extends ReflectiveSoapSerializable {
    private String projektName;
    private String projektToken;
    private Long creatorId;
    private Long responsiblePersonId;
    private String info;

    public String getProjektName() {
        return projektName;
    }

    public void setProjektName(final String projektName) {
        this.projektName = projektName;
    }

    public String getProjektToken() {
        return projektToken;
    }

    public void setProjektToken(final String projektToken) {
        this.projektToken = projektToken;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(final Long creatorId) {
        this.creatorId = creatorId;
    }

    public Long getResponsiblePersonId() {
        return responsiblePersonId;
    }

    public void setResponsiblePersonId(final Long responsiblePersonId) {
        this.responsiblePersonId = responsiblePersonId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(final String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PlannedProject");
        sb.append("{id=").append(id);
        sb.append(", projektName='").append(projektName).append('\'');
        sb.append(", projektToken='").append(projektToken).append('\'');
        sb.append(", creatorId=").append(creatorId);
        sb.append(", responsiblePersonId=").append(responsiblePersonId);
        sb.append(", info='").append(info).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
