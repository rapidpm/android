package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

import java.util.Date;

/**
 * User: Alexander Vos
 * Date: 17.12.12
 * Time: 16:13
 */
public class IssueComment extends ReflectiveSoapSerializable {
    private String text;
    private Long creatorId;
    private Date created;

    public IssueComment() {
    }

    public IssueComment(final String text, final Long creatorId) {
        this.text = text;
        this.creatorId = creatorId;
        this.created = new Date();
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(final Long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssueComment");
        sb.append("{id=").append(id);
        sb.append(", text='").append(text).append('\'');
        sb.append(", creatorId=").append(creatorId);
        sb.append(", created=").append(created);
        sb.append('}');
        return sb.toString();
    }
}
