package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

/**
 * User: Alexander Vos
 * Date: 18.12.12
 * Time: 11:10
 */
public class IssueStatus extends ReflectiveSoapSerializable {
    private Long projectId;
    private String statusName;
    private String statusFileName;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(final Long projectId) {
        this.projectId = projectId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(final String statusName) {
        this.statusName = statusName;
    }

    public String getStatusFileName() {
        return statusFileName;
    }

    public void setStatusFileName(final String statusFileName) {
        this.statusFileName = statusFileName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssueStatus");
        sb.append("{id=").append(id);
        sb.append(", projectId=").append(projectId);
        sb.append(", statusName='").append(statusName).append('\'');
        sb.append(", statusFileName='").append(statusFileName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
