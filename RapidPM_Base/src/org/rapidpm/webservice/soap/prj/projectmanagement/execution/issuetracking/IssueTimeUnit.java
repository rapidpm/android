package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.DefaultValue;
import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

import java.util.Date;

/**
 * User: Alexander Vos
 * Date: 17.12.12
 * Time: 16:13
 */
public class IssueTimeUnit extends ReflectiveSoapSerializable {
    private Date date;
    @DefaultValue("0")
    private int minutes;
    private String comment;
    private Long workerId;

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(final int minutes) {
        this.minutes = minutes;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }

    public Long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(final Long workerId) {
        this.workerId = workerId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssueTimeUnit");
        sb.append("{id=").append(id);
        sb.append(", date=").append(date);
        sb.append(", minutes=").append(minutes);
        sb.append(", comment='").append(comment).append('\'');
        sb.append(", workerId=").append(workerId);
        sb.append('}');
        return sb.toString();
    }
}
