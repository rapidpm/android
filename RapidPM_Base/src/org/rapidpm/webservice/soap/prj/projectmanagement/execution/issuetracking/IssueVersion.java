package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.rapidpm.webservice.soap.impl.ReflectiveSoapSerializable;

/**
 * User: Alexander Vos
 * Date: 18.12.12
 * Time: 11:11
 */
public class IssueVersion extends ReflectiveSoapSerializable {
    private Long projectId;
    private String versionName;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(final Long projectId) {
        this.projectId = projectId;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(final String versionName) {
        this.versionName = versionName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IssueVersion");
        sb.append("{id=").append(id);
        sb.append(", projectId=").append(projectId);
        sb.append(", versionName='").append(versionName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
