package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.ksoap2.serialization.SoapObject;
import org.rapidpm.webservice.soap.BaseService;
import org.rapidpm.webservice.soap.util.SoapExecutor;
import org.rapidpm.webservice.soap.util.SoapHelper;

import java.io.IOException;
import java.util.List;

/**
 * User: Alexander Vos
 * Date: 13.12.12
 * Time: 15:28
 */
public class IssueTrackingService extends BaseService<IssueBase> {
    public IssueTrackingService() {
        super("IssueTrackingWS", "http://issuetracking.execution.projectmanagement.prj.persistence.webservice.rapidpm.org/", IssueBase.class);
    }

    public List<IssuePriority> getAllIssuePriorities(final Long projectId) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getAllIssuePriorities", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("projectId", projectId);
            }
        }.execute(IssuePriority.class);
    }

    public List<IssueStatus> getAllIssueStatuses(final Long projectId) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getAllIssueStatuses", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("projectId", projectId);
            }
        }.execute(IssueStatus.class);
    }

    public List<IssueType> getAllIssueTypes(final Long projectId) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getAllIssueTypes", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("projectId", projectId);
            }
        }.execute(IssueType.class);
    }

    public List<IssueVersion> getAllIssueVersions(final Long projectId) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getAllIssueVersions", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("projectId", projectId);
            }
        }.execute(IssueVersion.class);
    }

    public IssueBase findIssueById(final Long id) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "findIssueById", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("id", id);
            }
        }.executeSingle(IssueBase.class);
    }

    public List<IssueBase> findIssuesByIdList(final List<Long> idList) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "findIssuesByIdList", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                SoapHelper.addValueCollection(request, "idList", idList);
            }
        }.execute(IssueBase.class);
    }

    public <T> T getIssueAttribute(final Long id, final String name, final Class<T> type) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getIssueAttribute", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("id", id);
                request.addProperty("name", name);
            }
        }.executeSingle(type);
    }

    public Long saveIssue(final IssueBase issue) throws IOException {
        final Long id = new SoapExecutor(relativeWebServiceURL, "saveIssue", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                SoapHelper.addObject(request, "issue", issue);
            }
        }.executeSingle(Long.class);
        issue.setId(id);
        return id;
    }

    public boolean deleteIssue(final Long id) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "deleteIssue", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("id", id);
            }
        }.executeSingle(Boolean.class);
    }

    public List<IssueBase> getSubIssues(final Long issueId) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getSubIssues", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("issueId", issueId);
            }
        }.execute(IssueBase.class);
    }

    public Long addSubIssue(final Long issueId, final IssueBase subIssue) throws IOException {
        final Long id = new SoapExecutor(relativeWebServiceURL, "addSubIssue", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("issueId", issueId);
                SoapHelper.addObject(request, "subIssue", subIssue);
            }
        }.executeSingle(Long.class);
        subIssue.setId(id);
        return id;
    }

    public List<IssueBase> getTopLevelIssues(final Long projectId) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "getTopLevelIssues", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("projectId", projectId);
            }
        }.execute(IssueBase.class);
    }

    public Long addOrChangeComment(final Long issueId, final IssueComment comment) throws IOException {
        final Long id = new SoapExecutor(relativeWebServiceURL, "addOrChangeComment", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("issueId", issueId);
                SoapHelper.addObject(request, "comment", comment);
            }
        }.executeSingle(Long.class);
        comment.setId(id);
        return id;
    }

    public boolean deleteComment(final Long issueId, final Long commentId) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "deleteComment", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("issueId", issueId);
                request.addProperty("commentId", commentId);
            }
        }.executeSingle(Boolean.class);
    }

    public List<IssueSearchSuggestion> search(final Long projectId, final String query, final int maxResults) throws IOException {
        return new SoapExecutor(relativeWebServiceURL, "search", namespace) {
            @Override
            protected void fillRequest(final SoapObject request) {
                request.addProperty("projectId", projectId);
                request.addProperty("query", query);
                request.addProperty("maxResults", maxResults);
            }
        }.execute(IssueSearchSuggestion.class);
    }

}
