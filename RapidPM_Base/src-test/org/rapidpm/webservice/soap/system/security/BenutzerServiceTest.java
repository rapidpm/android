package org.rapidpm.webservice.soap.system.security;

import org.junit.Test;
import org.ksoap2.SoapFault;
import org.rapidpm.webservice.soap.BaseServiceTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: Alexander Vos
 * Date: 09.12.12
 * Time: 15:10
 */
public class BenutzerServiceTest extends BaseServiceTest<BenutzerService> {
    public BenutzerServiceTest() {
        super(BenutzerService.class);
    }

    @Test
    public void testFindById() throws Exception {
        final Benutzer benutzer = service.findById(6L);
        assertEquals(new Long(6L), benutzer.getId());
    }

    @Test
    public void testFindByIdList() throws Exception {
        final List<Benutzer> benutzerList = service.findByIdList(Arrays.asList(2L, 3L, 6L));
        assertEquals(3, benutzerList.size());
    }

    @Test
    public void testGetAll() throws Exception {
        final List<Benutzer> benutzerList = service.getAll();
        System.out.println(benutzerList);
    }

    @Test
    public void testAuthenticate() throws Exception {
        Benutzer benutzer = null;
        try {
            benutzer = service.authenticate("alexander.vos", "geheim");
        } catch (SoapFault sf) {
            System.out.println(sf.getMessage());
        }

        System.out.println(benutzer);

        assertNotNull(benutzer);
        assertEquals(new Long(6L), benutzer.getId());
        assertEquals("alexander.vos", benutzer.getLogin());
        assertEquals("alexander.vos@rapidpm.org", benutzer.getEmail());
        assertEquals(1, benutzer.getRollen().size());
        assertEquals("admin", benutzer.getRollen().iterator().next().getName());
    }

    @Test
    public void testLogout() throws Exception {
        testAuthenticate();
        service.logout();
    }
}
