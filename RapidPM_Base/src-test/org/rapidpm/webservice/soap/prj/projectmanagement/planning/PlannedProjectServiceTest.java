package org.rapidpm.webservice.soap.prj.projectmanagement.planning;

import org.junit.Test;
import org.rapidpm.webservice.soap.BaseServiceTest;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * User: Alexander Vos
 * Date: 13.12.12
 * Time: 16:11
 */
public class PlannedProjectServiceTest extends BaseServiceTest<PlannedProjectService> {
    public PlannedProjectServiceTest() {
        super(PlannedProjectService.class);
    }

    @Test
    public void testFindById() throws Exception {
        final PlannedProject project = service.findById(1L);
        System.out.println(project);
        assertEquals(new Long(1L), project.getId());
    }

    @Test
    public void testGetAll() throws Exception {
        final List<PlannedProject> projects = service.getAll();
        System.out.println(projects);
    }

    @Test
    public void testGetFirstProject() throws Exception {
        final PlannedProject project = service.getFirstProject();
        System.out.println(project);
    }
}
