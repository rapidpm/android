package org.rapidpm.webservice.soap.prj.projectmanagement.execution.issuetracking;

import org.junit.Test;
import org.rapidpm.webservice.soap.BaseServiceTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * User: Alexander Vos
 * Date: 13.12.12
 * Time: 16:11
 */
public class IssueTrackingServiceTest extends BaseServiceTest<IssueTrackingService> {
    public IssueTrackingServiceTest() {
        super(IssueTrackingService.class);
    }

    @Test
    public void testGetAllIssuePriorities() throws Exception {
        final List<IssuePriority> issuePriorities = service.getAllIssuePriorities(1L);
        System.out.println(issuePriorities);
        assertFalse(issuePriorities.isEmpty());
    }

    @Test
    public void testGetAllIssueStatuses() throws Exception {
        final List<IssueStatus> issueStatuses = service.getAllIssueStatuses(1L);
        System.out.println(issueStatuses);
        assertFalse(issueStatuses.isEmpty());
    }

    @Test
    public void testGetAllIssueTypes() throws Exception {
        final List<IssueType> issueTypes = service.getAllIssueTypes(1L);
        System.out.println(issueTypes);
        assertFalse(issueTypes.isEmpty());
    }

    @Test
    public void testGetAllIssueVersions() throws Exception {
        final List<IssueVersion> issueVersions = service.getAllIssueVersions(1L);
        System.out.println(issueVersions);
        assertFalse(issueVersions.isEmpty());
    }

    @Test
    public void testFindIssueById() throws Exception {
        final IssueBase issue = service.findIssueById(124L);
        assertNotNull(issue);
    }

    @Test
    public void testFindIssuesByIdList() throws Exception {
        final List<IssueBase> issueList = service.findIssuesByIdList(Arrays.asList(43L, 45L, 47L));
        assertEquals(3, issueList.size());
    }

    @Test
    public void testGetIssueAttribute() throws Exception {
        final String text = service.getIssueAttribute(124L, "text", String.class);
        assertEquals("PRO1-14", text);
    }

    @Test
    public void testSaveIssue() throws Exception {
        final IssueBase issue = new IssueBase();
        issue.setProjectId(1L);
        issue.setSummary("test issue");
        issue.setStory("epic story is epic");
        issue.setReporterId(6L);
        issue.setAssigneeId(4L);
        issue.setTypeId(11L);
        issue.setPriorityId(24L);
        issue.setStatusId(17L);
        issue.setVersionId(30L);
        issue.setRisk(10);
        issue.setDueDate_planned(new Date());
        issue.setDueDate_resolved(new Date());
        issue.setDueDate_closed(new Date());

        final List<IssueComment> comments = new ArrayList<IssueComment>();
        comments.add(new IssueComment("comment 1", 6L));
        comments.add(new IssueComment("comment 2", 4L));
        issue.setComments(comments);

        final Long id = service.saveIssue(issue);
        assertNotNull(id);
    }

    @Test
    public void testDeleteIssue() throws Exception {
        final boolean success = service.deleteIssue(123L);
        assertTrue(success);
    }

    @Test
    public void testGetSubIssues() throws Exception {
        final List<IssueBase> subIssues = service.getSubIssues(50L);
        assertEquals(4, subIssues.size());
    }

    @Test
    public void testAddSubIssue() throws Exception {
        final IssueBase subIssue = new IssueBase();
        subIssue.setSummary("sub issue test");
        subIssue.setReporterId(6L);

        final Long id = service.addSubIssue(122L, subIssue);
        assertNotNull(id);
    }

    @Test
    public void testGetTopLevelIssues() throws Exception {
        final List<IssueBase> issues = service.getTopLevelIssues(2L);
        System.out.println(issues);
        assertEquals(3, issues.size());
    }

    @Test
    public void testAddOrChangeComment() throws Exception {
        final IssueComment comment = new IssueComment("Awesome comment is awesome!", 6L);
        final Long id = service.addOrChangeComment(122L, comment);
        assertNotNull(id);
    }

    @Test
    public void testDeleteComment() throws Exception {
        final boolean success = service.deleteComment(50L, 71L);
        assertTrue(success);
    }

    @Test
    public void testSearch() throws Exception {
        final List<IssueSearchSuggestion> searchResults = service.search(1L, "PRO", 10);
        assertFalse(searchResults.isEmpty());
    }
}
