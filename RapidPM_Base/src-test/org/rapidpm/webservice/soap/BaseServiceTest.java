package org.rapidpm.webservice.soap;

import org.junit.BeforeClass;
import org.rapidpm.webservice.soap.util.SoapExecutor;

/**
 * User: Alexander Vos
 * Date: 13.12.12
 * Time: 16:16
 */
public class BaseServiceTest<T extends BaseService> {
    protected T service;

    @BeforeClass
    public static void init() throws Exception {
        SoapExecutor.setServerURL("http://localhost:8080/rapidpmws/webservice/");
    }

    public BaseServiceTest(final Class<T> serviceClass) {
        try {
            service = serviceClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
